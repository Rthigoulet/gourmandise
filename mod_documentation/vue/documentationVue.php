<?php

$tpl = new Smarty();
$tpl->assign('title','Gourmandise | Documentations');
$tpl->assign('nomAffiche',$_SESSION['prenom']. ' ' .$_SESSION['nom']);
$tpl->assign('titlePage',"Test");
$tpl->assign("titreForm", "Table : Documentations");
$tpl->assign("titleContenu", " La Documentation, ça se Partage !");

/*Tables Contenu*/
$tpl->assign("btnSee", "Voir");
$tpl->assign("btnDll", "Télécharger");
$tpl->assign("titreTab1", "Base de données");
$tpl->assign("contenu1Tab1", "Voici la base de données téléchargeable et utilisable");
$tpl->assign("contenu2Tab1", "Voici le Modele de la base de données");
$tpl->assign("titreTab2", "Cahier des Charges");
$tpl->assign("contenu1Tab2", "Voici le Cahier des Charges Consultable qui inclut les maquettes");
$tpl->assign("titreTab3", "Code du Projet");
$tpl->assign("contenu1Tab3", "Consultation du Code du Projet via Gitlab");
/*Tables Contenu*/

if (isset($parametre['action'])) {

    switch ($parametre['action']) {
        
    }

    $tpl->display('mod_documentation/vue/documentationVue.tpl');
} else {

    if (isset($message)) {
        $tpl->assign('messageErreur', $message);
    } else {
        $tpl->assign('messageErreur', '');
    }
    $tpl->display('mod_documentation/vue/documentationVue.tpl');
}