
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />

        <title>Gentelella Alela! | </title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>


    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                {include file='template/production/leftNavBar.tpl'}
                {include file='template/production/topNavBar.tpl'}
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Listes des Clients</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <form class="pos-ajout" method="POST" action="index.php">
                                    <input type="hidden" name="gestion" value="client">
                                    <input type="hidden" name="action" value="form_ajouter">
                                    <label>Ajouter un Client : <input id="aImage" type="image" name="btn_ajouter" src='template/images/icones/a16.png'></label>
                                </form>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Code Client</th>
                                        <th>Nom et Prénom</th>
                                        <th>Ville</th>
                                        <th>Téléphone</th>
                                        <th class="pos-actions">Consulter</th>
                                        <th class="pos-actions">Modifier</th>
                                        <th class="pos-actions">Supprimer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach from=$listeClients item=client}

                                    <tr>
                                        <td>{$client.code_c}</td>
                                        <td>{$client.nom}</td>
                                        <td>{$client.ville}</td>
                                        <td>{$client.telephone}</td>
                                        <td class="pos-actions">
                                            <form method="POST" action="index.php">
                                                <input type="hidden" name="gestion" value="client">
                                                <input type="hidden" name="action" value="form_consulter">
                                                <input type="hidden" name="f_code_c" value="{$client.code_c}">
                                                <input id="pImage" type="image" name="btn_consulter" src='template/images/icones/p16.png'>
                                            </form>
                                        </td>
                                        <td class="pos-actions">
                                            <form method="POST" action="index.php">
                                                <input type="hidden" name="gestion" value="client">
                                                <input type="hidden" name="action" value="form_modifier">
                                                <input type="hidden" name="f_code_c" value="{$client.code_c}">
                                                <input id="mImage" type="image" name="btn_modifier" src='template/images/icones/m16.png'>
                                            </form>
                                        </td>
                                        <td class="pos-actions">
                                            <form method="POST" action="index.php">
                                                <input type="hidden" name="gestion" value="client">
                                                <input type="hidden" name="action" value="form_supprimer">
                                                <input type="hidden" name="f_code_c" value="{$client.code_c}">
                                                <input id="sImage" type="image" name="btn_supprimer" src='template/images/icones/s16.png'>
                                            </form>
                                        </td>
                                    </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {include 'template/production/footerBar.tpl'}
        <!-- jQuery -->
        <script src="template/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="template/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="template/vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="template/vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="template/vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="template/vendors/iCheck/icheck.min.js"></script>
        <!-- Skycons -->
        <script src="template/vendors/skycons/skycons.js"></script>
        <!-- Flot -->
        <script src="template/vendors/Flot/jquery.flot.js"></script>
        <script src="template/vendors/Flot/jquery.flot.pie.js"></script>
        <script src="template/vendors/Flot/jquery.flot.time.js"></script>
        <script src="template/vendors/Flot/jquery.flot.stack.js"></script>
        <script src="template/vendors/Flot/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="template/vendors/flot.curvedlines/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="template/vendors/DateJS/build/date.js"></script>
        <!-- JQVMap -->
        <script src="template/vendors/jqvmap/dist/jquery.vmap.js"></script>
        <script src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="template/vendors/moment/min/moment.min.js"></script>
        <script src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="template/build/js/custom.min.js"></script>

    </body>
</html>