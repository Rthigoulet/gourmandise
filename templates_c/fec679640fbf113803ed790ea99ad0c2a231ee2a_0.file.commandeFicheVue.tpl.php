<?php
/* Smarty version 3.1.29, created on 2019-03-06 12:04:05
  from "C:\xampp\htdocs\gourmandise\mod_commande\vue\commandeFicheVue.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c7fa9252c83e8_63708776',
  'file_dependency' => 
  array (
    'fec679640fbf113803ed790ea99ad0c2a231ee2a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_commande\\vue\\commandeFicheVue.tpl',
      1 => 1551870242,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/production/leftNavBar.tpl' => 1,
    'file:template/production/topNavBar.tpl' => 1,
    'file:template/production/footerBar.tpl' => 1,
  ),
),false)) {
function content_5c7fa9252c83e8_63708776 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="template/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/leftNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <div class="main_container">
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/topNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <div class="right_col" role="main">
                    <div class="">

                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-sm-4">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=commande">Commandes</a></li>
                                            <li class="active"><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre-->        
                        <?php if ($_smarty_tpl->tpl_vars['valAction']->value != 'ajouter') {?>
                            <!--Fiche Commande-->
                            <div class="row">
                                <div class=" pull-left col-md-5 col-sm-12 col-xs-12 ">
                                    <div class="x_panel fixed_height_320">
                                        <div class="x_title">
                                            <h2><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div style="text-align: center;">
                                            <br />
                                            <form action="index.php" method="POST" class="form-horizontal">
                                                <div class="form-group">
                                                    <h2><b>Numero</b> : <?php echo $_smarty_tpl->tpl_vars['numero']->value;?>
</h2>
                                                </div>
                                                <div class="form-group">
                                                    <h2><b>Vendeur</b> : <?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</h2>
                                                </div>
                                                <div class="form-group">
                                                    <h2><b>Code Client</b> : <?php echo $_smarty_tpl->tpl_vars['code_c']->value;?>
</h2>
                                                </div>
                                                <div class="form-group">
                                                    <h2><b>Nom du Client</b> : <?php echo $_smarty_tpl->tpl_vars['nom']->value;?>
</h2>
                                                </div>
                                                <div class="ln_solid"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                                <!--BTN VALIDATION-->
                                <div class="col-md-2 col-sm-12 col-xs-12">
                                    <?php if ($_smarty_tpl->tpl_vars['valAction']->value == '') {?>
                                        <?php if ($_smarty_tpl->tpl_vars['valide']->value == 0) {?>
                                            <input type="submit" id="f_btn-action" class="col-md-12 col-sm-12 col-xs-12 btn btn-submit btn-dark" value="<?php echo $_smarty_tpl->tpl_vars['btnValide']->value;?>
">
                                        <?php } else { ?>
                                            <input type="submit" id="f_btn-action" class="col-md-12 col-sm-12 col-xs-12 btn btn-submit btn-dark" value="<?php echo $_smarty_tpl->tpl_vars['btnInvalide']->value;?>
">
                                        <?php }?>
                                    <?php }?>
                                </div>
                                <!--BTN VALIDATION-->


                                <!--Fiche Commande-->
                                <!--Fiche ETAT-->
                                <div class="pull-right col-md-5 col-sm-12 col-xs-12 ">
                                    <div class="x_panel fixed_height_320">
                                        <div class="x_title">
                                            <h2><?php echo $_smarty_tpl->tpl_vars['titreEtat']->value;?>
</h2>
                                            <a class="pull-right"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</a>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div style="text-align: center;">
                                            <br />
                                            <form action="index.php" method="POST" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                                <input type="hidden" name="gestion"  value="commande" >
                                                <input type="hidden" name="action"  value="<?php echo $_smarty_tpl->tpl_vars['valAction']->value;?>
" >


                                                <input type="hidden" name="f_numero"  class="form-control" readonly="readonly" value="<?php echo $_smarty_tpl->tpl_vars['numero']->value;?>
">

                                                <div class="form-group">
                                                    <h2><b>Date Commande</b> : <?php echo $_smarty_tpl->tpl_vars['date_commande']->value;?>
</h2>
                                                </div>


                                                <!--Seule endroit qui modifie-->
                                                
                                                <div class="form-group">
                                                    <h2><b>Date Livraison</b> : <?php echo $_smarty_tpl->tpl_vars['date_livraison']->value;?>
</h2>
                                                </div>
                                                
                                                <!--Seule endroit qui modifie-->    


                                                <div class="form-group">
                                                    <h2><b>Total HT</b> : <?php echo $_smarty_tpl->tpl_vars['total_ht']->value;?>
 € HT</h2>
                                                </div>
                                                <div class="form-group">
                                                    <h2><b>Commande Validée</b> : 
                                                        <?php ob_start();
echo $_smarty_tpl->tpl_vars['valide']->value;
$_tmp1=ob_get_clean();
if ($_tmp1 == "1") {?>
                                                            OUI
                                                        <?php } else { ?>
                                                            NON
                                                        <?php }?>
                                                    </h2>
                                                </div>
                                                <div class="ln_solid">
                                                    
                                                </div>                                     
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--Fiche ETAT-->


                                <!--Fiche PRIX TOTAL COMMANDE-->
                                <div class="row">
                                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-euro"></i>
                                            </div>
                                            <div class="count"><?php echo $_smarty_tpl->tpl_vars['total_ttc']->value;?>
</div>

                                            <h3>Montant Total de la Commande</h3>
                                            <p>Toute Taxe Comprise en €</p>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-caret-square-o-down"></i>
                                            </div>
                                            <div class="count"><?php echo $_smarty_tpl->tpl_vars['total_ht']->value;?>
</div>

                                            <h3>Montant Hors-Taxe de la Commande</h3>
                                            <p>Hors-Taxe en €</p>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-caret-square-o-up"></i>
                                            </div>
                                            <div class="count"><?php echo $_smarty_tpl->tpl_vars['total_tva']->value;?>
</div>

                                            <h3>Montant de la TVA de la Commande</h3>
                                            <p>Taxe sur la Valeur Ajoutée en €</p>
                                        </div>
                                    </div>
                                </div>
                                <!--Fiche PRIX TOTAL COMMANDE-->

                                <!--Fiche Ligne-->
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Détail de la Commande</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <table id="datatable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>N°Ligne</th>
                                                        <th>Référence</th>
                                                        <th>Désignation</th>
                                                        <th>Quantité</th>
                                                        <th>Prix HT en €</th>
                                                            <?php if ($_smarty_tpl->tpl_vars['valAction']->value == "modifier") {?>
                                                            <th>Modifier ?</th>
                                                            <?php }?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['listeLigneCommande']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ligneC_0_saved_item = isset($_smarty_tpl->tpl_vars['ligneC']) ? $_smarty_tpl->tpl_vars['ligneC'] : false;
$_smarty_tpl->tpl_vars['ligneC'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['ligneC']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['ligneC']->value) {
$_smarty_tpl->tpl_vars['ligneC']->_loop = true;
$__foreach_ligneC_0_saved_local_item = $_smarty_tpl->tpl_vars['ligneC'];
?>
                                                        <tr>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['ligneC']->value['numero_ligne'];?>
</td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['ligneC']->value['reference'];?>
</td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['ligneC']->value['designation'];?>
</td>
                                                            <!--Seule endroit qui modifie-->
                                                            <?php if ($_smarty_tpl->tpl_vars['valAction']->value == "modifier") {?>
                                                                <td>
                                                                    <div class="form-group">
                                                                        <label for="quantite_demandee"></label>
                                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                            <input type="hidden" name="f_numero_ligne"  class="form-control" readonly="readonly" value="<?php echo $_smarty_tpl->tpl_vars['ligneC']->value['numero_ligne'];?>
"><!--Indispensable pour savoir quelle numero de ligne est sélectionné-->
                                                                            <input name="f_quantite_demandee" class="form-control col-md-2 col-xs-2" value="<?php echo $_smarty_tpl->tpl_vars['ligneC']->value['quantite_demandee'];?>
" <?php echo $_smarty_tpl->tpl_vars['readonlyON']->value;?>
 type="number" > 
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            <?php } else { ?>
                                                                <td><?php echo $_smarty_tpl->tpl_vars['ligneC']->value['quantite_demandee'];?>
</td>
                                                            <?php }?>
                                                            <!--Seule endroit qui modifie-->
                                                            <td><?php echo $_smarty_tpl->tpl_vars['ligneC']->value['prix_unitaire_ht'];?>
</td>

                                                            <?php if ($_smarty_tpl->tpl_vars['valAction']->value == "modifier") {?>
                                                                <td>
                                                                    <div class="form-group">

                                                                        <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="<?php echo $_smarty_tpl->tpl_vars['valBtnAction']->value;?>
">

                                                                    </div>
                                                                </td>
                                                            <?php }?>
                                                        </tr>
                                                    <?php
$_smarty_tpl->tpl_vars['ligneC'] = $__foreach_ligneC_0_saved_local_item;
}
if ($__foreach_ligneC_0_saved_item) {
$_smarty_tpl->tpl_vars['ligneC'] = $__foreach_ligneC_0_saved_item;
}
?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--Fiche Ligne-->
                                <div>                 
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=commande"'>
                                            <button class="btn btn-info pull-right" onclick="window.print();"><i class="fa fa-print"></i> Imprimer</button>
                                        </div>
                                    </div>

                                </div> 

                            </div>
                        <?php } else { ?>











                            <!--AJOUTER-->
                            <div class="">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Listes des Produits</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <table id="datatable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Référence</th>
                                                        <th>Désignation</th>                                                  
                                                        <th>Prix Unitaire HT en €</th>
                                                        <th>Stock</th>                                                        
                                                        <th>Quantité</th>
                                                        <th class="pos-actions">Ajouter</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
$_from = $_smarty_tpl->tpl_vars['commandeListeProduit']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_listeCommande_1_saved_item = isset($_smarty_tpl->tpl_vars['listeCommande']) ? $_smarty_tpl->tpl_vars['listeCommande'] : false;
$_smarty_tpl->tpl_vars['listeCommande'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['listeCommande']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['listeCommande']->value) {
$_smarty_tpl->tpl_vars['listeCommande']->_loop = true;
$__foreach_listeCommande_1_saved_local_item = $_smarty_tpl->tpl_vars['listeCommande'];
?>
                                                        <tr>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['listeCommande']->value['reference'];?>
</td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['listeCommande']->value['designation'];?>
</td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['listeCommande']->value['prix_unitaire_HT'];?>
</td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['listeCommande']->value['stock'];?>
</td>
                                                            <td>
                                                                <input name="f_quantite_prod" class="form-control col-md-7 col-xs-12" value="" type="quantite_prod" >
                                                            </td>
                                                            <td class="pos-actions">
                                                                <form method="POST" action="index.php">
                                                                    <input type="hidden" name="gestion" value="commande">
                                                                    <input type="hidden" name="action" value="form_ajouter">
                                                                    <input type="hidden" name="f_reference" value="<?php echo $_smarty_tpl->tpl_vars['listeCommande']->value['reference'];?>
">
                                                                    <input id="pImage" type="image" name="btn_ajouter" src='template/images/icones/m16.png'>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    <?php
$_smarty_tpl->tpl_vars['listeCommande'] = $__foreach_listeCommande_1_saved_local_item;
}
if ($__foreach_listeCommande_1_saved_item) {
$_smarty_tpl->tpl_vars['listeCommande'] = $__foreach_listeCommande_1_saved_item;
}
?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        <!--AJOUTER-->












                        <br>
                        <br>
                        <br>
                        <br>

                    </div>
                </div>
            </div>
        </div>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/footerBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
    <!-- jQuery -->
    <?php echo '<script'; ?>
 src="template/vendors/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
    <!-- Bootstrap -->
    <?php echo '<script'; ?>
 src="template/vendors/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <!-- FastClick -->
    <?php echo '<script'; ?>
 src="template/vendors/fastclick/lib/fastclick.js"><?php echo '</script'; ?>
>
    <!-- NProgress -->
    <?php echo '<script'; ?>
 src="template/vendors/nprogress/nprogress.js"><?php echo '</script'; ?>
>
    <!-- Chart.js -->
    <?php echo '<script'; ?>
 src="template/vendors/Chart.js/dist/Chart.min.js"><?php echo '</script'; ?>
>
    <!-- gauge.js -->
    <?php echo '<script'; ?>
 src="template/vendors/gauge.js/dist/gauge.min.js"><?php echo '</script'; ?>
>
    <!-- bootstrap-progressbar -->
    <?php echo '<script'; ?>
 src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"><?php echo '</script'; ?>
>
    <!-- iCheck -->
    <?php echo '<script'; ?>
 src="template/vendors/iCheck/icheck.min.js"><?php echo '</script'; ?>
>
    <!-- Datatables -->
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.flash.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.html5.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.print.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/jszip/dist/jszip.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/pdfmake/build/pdfmake.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/pdfmake/build/vfs_fonts.js"><?php echo '</script'; ?>
>
    <!-- Custom Theme Scripts -->
    <!-- Skycons -->
    <?php echo '<script'; ?>
 src="template/vendors/skycons/skycons.js"><?php echo '</script'; ?>
>
    <!-- Flot -->
    <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.pie.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.time.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.stack.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.resize.js"><?php echo '</script'; ?>
>
    <!-- Flot plugins -->
    <?php echo '<script'; ?>
 src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/flot.curvedlines/curvedLines.js"><?php echo '</script'; ?>
>
    <!-- DateJS -->
    <?php echo '<script'; ?>
 src="template/vendors/DateJS/build/date.js"><?php echo '</script'; ?>
>
    <!-- JQVMap -->
    <?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/jquery.vmap.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"><?php echo '</script'; ?>
>
    <!-- bootstrap-daterangepicker -->
    <?php echo '<script'; ?>
 src="template/vendors/moment/min/moment.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"><?php echo '</script'; ?>
>

    <!-- Custom Theme Scripts -->
    <?php echo '<script'; ?>
 src="template/build/js/custom.min.js"><?php echo '</script'; ?>
>

</body>
</html>
<?php }
}
