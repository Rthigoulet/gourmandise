<?php
/* Smarty version 3.1.29, created on 2019-02-25 15:16:50
  from "C:\xampp\htdocs\gourmandise\template\production\topNavBar.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c73f8d2ea82a9_60768593',
  'file_dependency' => 
  array (
    '275934fdd6c8feb5b7f70e51bfefd5a1901e2926' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\template\\production\\topNavBar.tpl',
      1 => 1551100706,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c73f8d2ea82a9_60768593 ($_smarty_tpl) {
?>
<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <?php echo $_smarty_tpl->tpl_vars['nomAffiche']->value;?>

                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li>
                            <a href="index.php?gestion=profil&action=form_modifier"> <i class="fa fa-user pull-right"></i> Profil</a>
                        </li>
                        <li>
                            <a href="index.php?gestion=authentification&action=deconnexion"><i class="fa fa-sign-out pull-right"></i> Deconnexion</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation --><?php }
}
