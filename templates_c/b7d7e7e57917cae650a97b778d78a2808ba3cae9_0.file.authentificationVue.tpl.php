<?php
/* Smarty version 3.1.29, created on 2019-02-15 11:36:06
  from "C:\xampp\htdocs\gourmandise\mod_authentification\vue\authentificationVue.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c6696169b8fe8_97222745',
  'file_dependency' => 
  array (
    'b7d7e7e57917cae650a97b778d78a2808ba3cae9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_authentification\\vue\\authentificationVue.tpl',
      1 => 1550226963,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c6696169b8fe8_97222745 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- Animate.css -->
        <link href="template/vendors/animate.css/animate.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
        <link href="template/build/css/style.css" rel="stylesheet">
    </head>

    <body class="login">
        <div>
            <div class="login_wrapper">
                <div class="animate form login_form">
                    <a><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</a>
                    <section style="" class="login_content">
                        
                        <form method="POST" action="index.php">
                            
                            <h1>Connexion</h1>
                            <input type="hidden" name="action" value="authentification">  

                            <div>
                                <input type="text" class="form-control" placeholder="Login" required="" name="login"/>  
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Mot de Passe" required="" name="motDePasse"/> 
                            </div>
                            <div>
                                <input class="btn btn-dark btnConnexion" type="submit" value="Se Connecter" name="connexion"> 
                            </div>
                        </form>
                        <div class="clearfix"></div>

                        <div class="separator">

                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1><i class="fa fa-graduation-cap"></i> Gourmandise CCI</h1>
                                <p>BTS SIO <?php echo $_smarty_tpl->tpl_vars['date']->value;?>
</p>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
<?php }
}
