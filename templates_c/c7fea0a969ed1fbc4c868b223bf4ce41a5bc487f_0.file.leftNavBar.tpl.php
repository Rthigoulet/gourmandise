<?php
/* Smarty version 3.1.29, created on 2019-02-13 15:46:26
  from "C:\xampp\htdocs\gourmandise\template\production\leftNavBar.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c642dc2efad95_92660200',
  'file_dependency' => 
  array (
    'c7fea0a969ed1fbc4c868b223bf4ce41a5bc487f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\template\\production\\leftNavBar.tpl',
      1 => 1550069184,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c642dc2efad95_92660200 ($_smarty_tpl) {
?>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" >
            <a href="index.php" class="site_title"><i class="fa fa-home"></i> <span>Gourmandise</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_info">
                <span>Bienvenue</span>
                <h2><?php echo $_smarty_tpl->tpl_vars['nomAffiche']->value;?>
</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <div class="menu_section">
                    <ul class="nav side-menu">
                        <ul class="nav side-menu">
                            <li>
                                <a href="index.php"><i class="fa fa-home"></i>Accueil</a>
                            </li>

                            <li>
                                <a href="index.php?gestion=profil&action=form_modifier"><i class="fa fa-user"></i>Mon Profil</a>
                            </li>
                        </ul>
                </div>
                <div class="menu_section">
                    <h3>Administration</h3>
                    <ul class="nav side-menu">
                        <li>
                            <a><i class="fa fa-cube"></i> Produits <span class="fa fa-chevron-down"></a>
                            <ul class="nav child_menu">
                                <li><a href="index.php?gestion=produit">Liste des Produits</a></li>
                                <li><a href="index.php?gestion=produit&action=form_ajouter">Nouveau Produits</a></li>
                            </ul>
                        </li>
                        <li>
                            <a><i href="#" class="fa fa-users"></i> Clients <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="index.php?gestion=client">Listes des Clients</a></li>
                                <li><a href="index.php?gestion=client&action=form_ajouter">Nouveau Clients</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="menu_section">
                    <h3>Commandes</h3>
                    <ul class="nav side-menu">
                        <li>
                            <a><i class="fa fa-book"></i> Historique Commandes <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="index.php?gestion=commande">Toutes les commandes</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="index.php?gestion=commande&action=form_ajouter"><i class="fa fa-envelope"></i>Passer une Commande</a>
                        </li>
                    </ul>
                </div>
                <div class="menu_section">
                    <h3>Supléments</h3>
                    <ul class="nav side-menu">
                        <li>
                            <a href="index.php?gestion=documentation"><i class="fa fa-file-text"></i>Documentations</a>
                        </li>
                        <li>
                            <a href="template/production/form/icons.html"><i class="fa fa-cogs"></i>Test</a>
                        </li>
                    </ul>
                </div>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div><?php }
}
