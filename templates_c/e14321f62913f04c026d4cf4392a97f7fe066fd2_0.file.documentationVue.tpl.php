<?php
/* Smarty version 3.1.29, created on 2019-02-23 19:35:19
  from "C:\xampp\htdocs\gourmandise\mod_documentation\vue\documentationVue.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c7192670f3b52_88253486',
  'file_dependency' => 
  array (
    'e14321f62913f04c026d4cf4392a97f7fe066fd2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_documentation\\vue\\documentationVue.tpl',
      1 => 1550509469,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/production/topNavBar.tpl' => 1,
    'file:template/production/leftNavBar.tpl' => 1,
    'file:template/production/footerBar.tpl' => 1,
  ),
),false)) {
function content_5c7192670f3b52_88253486 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/topNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/leftNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <div class="right_col" role="main">
                    <div class="">
                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart-o"><?php echo $_smarty_tpl->tpl_vars['titleContenu']->value;?>
</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=documentation">Documentations</a></li>
                                            <li class="active"><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre--> 
                        <div class="clearfix"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel fixed_height_200">
                                <div class="x_title">
                                    <h2><i class="fa fa-bars"></i> <?php echo $_smarty_tpl->tpl_vars['titreTab1']->value;?>
</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-xs-3">
                                        <!-- required for floating -->
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs tabs-left">
                                            <li class="active"><a href="#home" data-toggle="tab">Version</a>
                                            </li>
                                            <li><a href="#profile" data-toggle="tab">Modele</a>
                                            </li>
                                        </ul>
                                        <br />
                                    </div>
                                    <div class="col-xs-9">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="home">
                                                <div>
                                                    <p class="lead">VERSION</p>
                                                    <p><?php echo $_smarty_tpl->tpl_vars['contenu1Tab1']->value;?>
</p>
                                                </div>
                                                <div>
                                                    <a href="documentation/gourmandise-sarl-V4.sql" download>
                                                        <button type="button" class="btn btn-dark btn-sm pull-right"><?php echo $_smarty_tpl->tpl_vars['btnDll']->value;?>
</button>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="profile">
                                                <div>
                                                    <p class="lead">MODELE</p>
                                                    <p><?php echo $_smarty_tpl->tpl_vars['contenu2Tab1']->value;?>
</p>
                                                </div>
                                                <div>
                                                    <form method="get" target="_blank" action="documentation/bdGourmandise.png">
                                                        <button type="submit" class="btn btn-dark btn-sm pull-right"><?php echo $_smarty_tpl->tpl_vars['btnSee']->value;?>
</button>
                                                    </form>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel fixed_height_200">
                                <div class="x_title">
                                    <h2><?php echo $_smarty_tpl->tpl_vars['titreTab2']->value;?>
</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <div>
                                        <p><?php echo $_smarty_tpl->tpl_vars['contenu1Tab2']->value;?>
</p>
                                    </div>
                                    <div>
                                        <form method="get" target="_blank" action="documentation/cahierDesCharges.pdf">
                                            <button type="submit" class="btn btn-dark btn-sm pull-right"><?php echo $_smarty_tpl->tpl_vars['btnSee']->value;?>
</button>
                                        </form>
                                        <br />
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>     
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel fixed_height_200">
                                <div class="x_title">
                                    <h2><?php echo $_smarty_tpl->tpl_vars['titreTab3']->value;?>
</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <div>
                                        <p><?php echo $_smarty_tpl->tpl_vars['contenu1Tab3']->value;?>
</p>
                                    </div>
                                    <div>
                                        <form method="get" target="_blank" action="https://gitlab.com/Rthigoulet/gourmandise">
                                            <button type="submit" class="btn btn-dark btn-sm pull-right"><?php echo $_smarty_tpl->tpl_vars['btnSee']->value;?>
</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/footerBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

</div>
</div>
</body>

<!-- jQuery -->
<?php echo '<script'; ?>
 src="template/vendors/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
<!-- Bootstrap -->
<?php echo '<script'; ?>
 src="template/vendors/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<!-- FastClick -->
<?php echo '<script'; ?>
 src="template/vendors/fastclick/lib/fastclick.js"><?php echo '</script'; ?>
>
<!-- NProgress -->
<?php echo '<script'; ?>
 src="template/vendors/nprogress/nprogress.js"><?php echo '</script'; ?>
>
<!-- Chart.js -->
<?php echo '<script'; ?>
 src="template/vendors/Chart.js/dist/Chart.min.js"><?php echo '</script'; ?>
>
<!-- gauge.js -->
<?php echo '<script'; ?>
 src="template/vendors/gauge.js/dist/gauge.min.js"><?php echo '</script'; ?>
>
<!-- bootstrap-progressbar -->
<?php echo '<script'; ?>
 src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"><?php echo '</script'; ?>
>
<!-- iCheck -->
<?php echo '<script'; ?>
 src="template/vendors/iCheck/icheck.min.js"><?php echo '</script'; ?>
>
<!-- Skycons -->
<?php echo '<script'; ?>
 src="template/vendors/skycons/skycons.js"><?php echo '</script'; ?>
>
<!-- Flot -->
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.pie.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.time.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.stack.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.resize.js"><?php echo '</script'; ?>
>
<!-- Flot plugins -->
<?php echo '<script'; ?>
 src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/flot.curvedlines/curvedLines.js"><?php echo '</script'; ?>
>
<!-- DateJS -->
<?php echo '<script'; ?>
 src="template/vendors/DateJS/build/date.js"><?php echo '</script'; ?>
>
<!-- JQVMap -->
<?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/jquery.vmap.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"><?php echo '</script'; ?>
>
<!-- bootstrap-daterangepicker -->
<?php echo '<script'; ?>
 src="template/vendors/moment/min/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"><?php echo '</script'; ?>
>

<!-- Custom Theme Scripts -->
<?php echo '<script'; ?>
 src="template/build/js/custom.min.js"><?php echo '</script'; ?>
>
</html>
<?php }
}
