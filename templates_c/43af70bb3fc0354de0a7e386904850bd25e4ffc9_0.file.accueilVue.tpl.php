<?php
/* Smarty version 3.1.29, created on 2019-03-06 11:27:08
  from "C:\xampp\htdocs\gourmandise\mod_accueil\vue\accueilVue.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c7fa07c887ef3_59591655',
  'file_dependency' => 
  array (
    '43af70bb3fc0354de0a7e386904850bd25e4ffc9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_accueil\\vue\\accueilVue.tpl',
      1 => 1551868026,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/production/topNavBar.tpl' => 1,
    'file:template/production/leftNavBar.tpl' => 1,
    'file:template/production/footerBar.tpl' => 1,
  ),
),false)) {
function content_5c7fa07c887ef3_59591655 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md footer">
        <div class="container body">
            <div class="main_container">
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/topNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/leftNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <div class="right_col" role="main">
                    <div class="">
                        <!--Titre-->
                        
                        <!--Titre-->
                        <!--Contenu 1.0 -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h3>Bonjour 
                                        <a href="index.php?gestion=profil&action=form_modifier"><strong><?php echo $_smarty_tpl->tpl_vars['nomAffiche']->value;?>
</strong></a>
                                        votre Chiffre d'affaires Global est de
                                        <strong><?php echo $_smarty_tpl->tpl_vars['votreCA']->value;?>
</strong> € HT 
                                        <a class="close-link pull-right"><i class="fa fa-close"></i></a>
                                    </h3> 
                                </div>                 
                            </div>
                        </div>
                        <!--Contenu 1.0 -->
                        <!--Contenu 1.1 -->
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>L'année 2017 en Chiffres !</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_panel">
                                    <h4> <a class='fa fa-users' href="index.php?gestion=client"></a>  Le nombre de Clients</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['nbClientsTotal2017']->value;?>
</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_panel">
                                    <h4><i class='fa fa-euro'></i>  Le chiffre d'affaires Global</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['chiffresAffaires2017']->value;?>
 € HT</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_panel">
                                    <h4><a class='fa fa-cubes' href="index.php?gestion=produit"></a>  Moyenne de produits par Commande</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['moyenneCommande2017']->value;?>
 Unité</h3>
                                </div>
                            </div>
                        </div>
                        <!--Contenu 1.1 -->

                        <!--Contenu 2-->
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>L'année 2018 en Chiffres !</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_panel">
                                    <h4> <a class='fa fa-users' href="index.php?gestion=client"></a>  Le nombre de Clients</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['nbClientsTotal2018']->value;?>
</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_panel">
                                    <h4><i class='fa fa-euro'></i>  Le chiffre d'affaires Global</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['chiffresAffaires2018']->value;?>
 € HT</h3>
                                </div>
                                <div class="x_panel">
                                    <h4><a class='fa fa-cubes' href="index.php?gestion=produit"></a>  Moyenne de produits par Commande</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['moyenneCommande2018']->value;?>
 Unité</h3>
                                </div>
                            </div>
                        </div>
                        <!--Contenu 2-->
                        <!--Contenu 3-->
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>L'année 2019 en Chiffres !</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_panel">
                                    <h4> <a class='fa fa-users' href="index.php?gestion=client"></a>  Le nombre de Clients</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['nbClientsTotal2019']->value;?>
</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_panel">
                                    <h4><i class='fa fa-euro'></i>  Le chiffre d'affaires Global</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['chiffresAffaires2019']->value;?>
 € HT</h3>
                                </div>
                                <div class="x_panel">
                                    <h4><a class='fa fa-cubes' href="index.php?gestion=produit"></a>  Moyenne de produits par Commande</h4>
                                    <h3><?php echo $_smarty_tpl->tpl_vars['moyenneCommande2019']->value;?>
 Unité</h3>
                                </div>
                            </div>
                        </div>
                        <!--Contenu 3-->

                        <!--Contenu 4 | CANVAS-->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel tile overflow_hidden">
                                <div class="x_title">
                                    <h2>Les Produits en vedette</h2>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table style="width:100%">
                                        <tr>
                                            <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <p>Top 8</p>
                                            </th>
                                            <th>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <p>Produits</p>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <canvas class="canvasDoughnut" height="270" width="270" style="margin: 15px 10px 10px 0"></canvas>
                                            </td>
                                            <td>
                                                <table class="tile_info">
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square blue"></i>IOS </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square green"></i>Android </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square aero"></i>Symbian </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square red"></i>Others </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--Contenu 4 | CANVAS-->


                        <!--Contenu 5-->
                        <!-- bar chart -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Le chiffre d'affaires des 8 meilleurs clients</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div id="graph_bar" style="width:100%; height:280px;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /bar charts -->
                    </div>

                </div>
            </div>
        </div>
        <!--Contenu 5-->
    </div>
</div>
</div>
</div>
<br>
<br>
</body>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/footerBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- jQuery -->
<?php echo '<script'; ?>
 src="template/vendors/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
<!-- Bootstrap -->
<?php echo '<script'; ?>
 src="template/vendors/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<!-- FastClick -->
<?php echo '<script'; ?>
 src="template/vendors/fastclick/lib/fastclick.js"><?php echo '</script'; ?>
>
<!-- NProgress -->
<?php echo '<script'; ?>
 src="template/vendors/nprogress/nprogress.js"><?php echo '</script'; ?>
>
<!-- Chart.js -->
<?php echo '<script'; ?>
 src="template/vendors/Chart.js/dist/Chart.min.js"><?php echo '</script'; ?>
>
<!-- gauge.js -->
<?php echo '<script'; ?>
 src="template/vendors/gauge.js/dist/gauge.min.js"><?php echo '</script'; ?>
>
<!-- bootstrap-progressbar -->
<?php echo '<script'; ?>
 src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"><?php echo '</script'; ?>
>
<!-- iCheck -->
<?php echo '<script'; ?>
 src="template/vendors/iCheck/icheck.min.js"><?php echo '</script'; ?>
>
<!-- Skycons -->
<?php echo '<script'; ?>
 src="template/vendors/skycons/skycons.js"><?php echo '</script'; ?>
>
<!-- Flot -->
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.pie.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.time.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.stack.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.resize.js"><?php echo '</script'; ?>
>
<!-- Flot plugins -->
<?php echo '<script'; ?>
 src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/flot.curvedlines/curvedLines.js"><?php echo '</script'; ?>
>
<!-- DateJS -->
<?php echo '<script'; ?>
 src="template/vendors/DateJS/build/date.js"><?php echo '</script'; ?>
>
<!-- JQVMap -->
<?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/jquery.vmap.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"><?php echo '</script'; ?>
>
<!-- bootstrap-daterangepicker -->
<?php echo '<script'; ?>
 src="template/vendors/moment/min/moment.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"><?php echo '</script'; ?>
>
<!-- morris.js -->
<?php echo '<script'; ?>
 src="template/vendors/raphael/raphael.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="template/vendors/morris.js/morris.min.js"><?php echo '</script'; ?>
>

<!-- Custom Theme Scripts -->
<?php echo '<script'; ?>
 src="template/build/js/custom.min.js"><?php echo '</script'; ?>
>
</html>
<?php }
}
