<?php
/* Smarty version 3.1.29, created on 2019-01-22 13:26:16
  from "C:\xampp\htdocs\gourmandise\mod_client\vue\clientStatVue.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c470be83fadc0_60945218',
  'file_dependency' => 
  array (
    '404c98716b3f9a33a0128c48cc2492f89d86f24c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_client\\vue\\clientStatVue.tpl',
      1 => 1548077733,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c470be83fadc0_60945218 ($_smarty_tpl) {
?>
<div class="col-md-6">
    <div class="card">
        <div class="card-header"> <strong>Statistiques</strong></div>
        <div class="card-body card-block">
            <div class="form-group"><strong>CA Réalisé : </strong><?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['ca']->value);?>
 €</div>
            <div class="form-group"><strong>Pourcentage du CA réalisé : </strong><?php echo sprintf("%.2f",$_smarty_tpl->tpl_vars['partCa']->value);?>
 %</div> 
            <div class="form-group"><strong>Ses meilleurs achats : </strong></label>
                <ul class="square">
                    <?php if ($_smarty_tpl->tpl_vars['listeProduits']->value == 0) {?>
                        <li> Aucun achat effectué.</li>
                        <?php } else { ?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['listeProduits']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_produit_0_saved_item = isset($_smarty_tpl->tpl_vars['produit']) ? $_smarty_tpl->tpl_vars['produit'] : false;
$_smarty_tpl->tpl_vars['produit'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['produit']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['produit']->value) {
$_smarty_tpl->tpl_vars['produit']->_loop = true;
$__foreach_produit_0_saved_local_item = $_smarty_tpl->tpl_vars['produit'];
?>
                            <li><?php echo $_smarty_tpl->tpl_vars['produit']->value['designation'];?>
 : <?php echo $_smarty_tpl->tpl_vars['produit']->value['nbProduits'];?>
</li>
                            <?php
$_smarty_tpl->tpl_vars['produit'] = $__foreach_produit_0_saved_local_item;
}
if ($__foreach_produit_0_saved_item) {
$_smarty_tpl->tpl_vars['produit'] = $__foreach_produit_0_saved_item;
}
?> 
                        <?php }?>

                </ul>

            </div>
        </div>
    </div>
</div>
<?php }
}
