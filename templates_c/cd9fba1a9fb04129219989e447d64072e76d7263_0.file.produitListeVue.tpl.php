<?php
/* Smarty version 3.1.29, created on 2019-02-25 15:17:14
  from "C:\xampp\htdocs\gourmandise\mod_produit\vue\produitListeVue.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c73f8ea69f251_70347874',
  'file_dependency' => 
  array (
    'cd9fba1a9fb04129219989e447d64072e76d7263' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_produit\\vue\\produitListeVue.tpl',
      1 => 1551100695,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/production/leftNavBar.tpl' => 1,
    'file:template/production/topNavBar.tpl' => 1,
    'file:template/production/footerBar.tpl' => 1,
  ),
),false)) {
function content_5c73f8ea69f251_70347874 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="template/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">

        <div class="container body">
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/leftNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <div class="main_container">

                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/topNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <div class="right_col" role="main">
                    <div class="">
                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-sm-4">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=produit">Produits</a></li>
                                            <li class="active"><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre-->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Listes des Produits</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <form class="pos-ajout" method="POST" action="index.php">
                                            <input type="hidden" name="gestion" value="produit">
                                            <input type="hidden" name="action" value="form_ajouter">
                                            <label>Ajouter un Produit : <input id="aImage" type="image" name="btn_ajouter" src='template/images/icones/a16.png'></label>
                                        </form>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">



                                    <table id="datatable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Référence</th>
                                                <th>Désignation</th>
                                                <th>Quantité</th>
                                                <th>Descriptif</th>
                                                <th>Prix Unitaire HT en €</th>
                                                <th>Stock</th>
                                                <th>Poids Pièce</th>
                                                <th class="pos-actions">Consulter</th>
                                                <th class="pos-actions">Modifier</th>
                                                <th class="pos-actions">Supprimer</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
$_from = $_smarty_tpl->tpl_vars['listeProduits']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_produit_0_saved_item = isset($_smarty_tpl->tpl_vars['produit']) ? $_smarty_tpl->tpl_vars['produit'] : false;
$_smarty_tpl->tpl_vars['produit'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['produit']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['produit']->value) {
$_smarty_tpl->tpl_vars['produit']->_loop = true;
$__foreach_produit_0_saved_local_item = $_smarty_tpl->tpl_vars['produit'];
?>
                                                <tr>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['reference'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['designation'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['quantite'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['descriptif'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['prix_unitaire_HT'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['stock'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['poids_piece'];?>
</td>
                                                    <td class="pos-actions">
                                                        <form method="POST" action="index.php">
                                                            <input type="hidden" name="gestion" value="produit">
                                                            <input type="hidden" name="action" value="form_consulter">
                                                            <input type="hidden" name="f_reference" value="<?php echo $_smarty_tpl->tpl_vars['produit']->value['reference'];?>
">
                                                            <input id="pImage" type="image" name="btn_consulter" src='template/images/icones/p16.png'>
                                                        </form>
                                                    </td>
                                                    <td class="pos-actions">
                                                        <form method="POST" action="index.php">
                                                            <input type="hidden" name="gestion" value="produit">
                                                            <input type="hidden" name="action" value="form_modifier">
                                                            <input type="hidden" name="f_reference" value="<?php echo $_smarty_tpl->tpl_vars['produit']->value['reference'];?>
">
                                                            <input id="mImage" type="image" name="btn_modifier" src='template/images/icones/m16.png'>
                                                        </form>
                                                    </td>
                                                    <td class="pos-actions">
                                                        <form method="POST" action="index.php">
                                                            <input type="hidden" name="gestion" value="produit">
                                                            <input type="hidden" name="action" value="form_supprimer">
                                                            <input type="hidden" name="f_reference" value="<?php echo $_smarty_tpl->tpl_vars['produit']->value['reference'];?>
">
                                                            <input id="sImage" type="image" name="btn_supprimer"  src='template/images/icones/s16.png'>
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php
$_smarty_tpl->tpl_vars['produit'] = $__foreach_produit_0_saved_local_item;
}
if ($__foreach_produit_0_saved_item) {
$_smarty_tpl->tpl_vars['produit'] = $__foreach_produit_0_saved_item;
}
?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/footerBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <!-- jQuery -->
        <?php echo '<script'; ?>
 src="template/vendors/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
        <!-- Bootstrap -->
        <?php echo '<script'; ?>
 src="template/vendors/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <!-- FastClick -->
        <?php echo '<script'; ?>
 src="template/vendors/fastclick/lib/fastclick.js"><?php echo '</script'; ?>
>
        <!-- NProgress -->
        <?php echo '<script'; ?>
 src="template/vendors/nprogress/nprogress.js"><?php echo '</script'; ?>
>
        <!-- Chart.js -->
        <?php echo '<script'; ?>
 src="template/vendors/Chart.js/dist/Chart.min.js"><?php echo '</script'; ?>
>
        <!-- gauge.js -->
        <?php echo '<script'; ?>
 src="template/vendors/gauge.js/dist/gauge.min.js"><?php echo '</script'; ?>
>
        <!-- bootstrap-progressbar -->
        <?php echo '<script'; ?>
 src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"><?php echo '</script'; ?>
>
        <!-- iCheck -->
        <?php echo '<script'; ?>
 src="template/vendors/iCheck/icheck.min.js"><?php echo '</script'; ?>
>
        <!-- Skycons -->
        <?php echo '<script'; ?>
 src="template/vendors/skycons/skycons.js"><?php echo '</script'; ?>
>
        <!-- Flot -->
        <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.pie.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.time.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.stack.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.resize.js"><?php echo '</script'; ?>
>
        <!-- Flot plugins -->
        <?php echo '<script'; ?>
 src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/flot.curvedlines/curvedLines.js"><?php echo '</script'; ?>
>
        <!-- DateJS -->
        <?php echo '<script'; ?>
 src="template/vendors/DateJS/build/date.js"><?php echo '</script'; ?>
>
        <!-- JQVMap -->
        <?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/jquery.vmap.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"><?php echo '</script'; ?>
>
        <!-- bootstrap-daterangepicker -->
        <?php echo '<script'; ?>
 src="template/vendors/moment/min/moment.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"><?php echo '</script'; ?>
>
        <!-- Datatables -->
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.flash.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.html5.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.print.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/jszip/dist/jszip.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/pdfmake/build/pdfmake.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/pdfmake/build/vfs_fonts.js"><?php echo '</script'; ?>
>

        <!-- Custom Theme Scripts -->
        <?php echo '<script'; ?>
 src="template/build/js/custom.min.js"><?php echo '</script'; ?>
>

    </body>
</html>
<?php }
}
