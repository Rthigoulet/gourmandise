<?php
/* Smarty version 3.1.29, created on 2019-02-25 15:17:35
  from "C:\xampp\htdocs\gourmandise\mod_commande\vue\commandeListeVue.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c73f8ff000e63_13451162',
  'file_dependency' => 
  array (
    'eae059b4daac0d25646142dd967e5c459c2afa20' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_commande\\vue\\commandeListeVue.tpl',
      1 => 1551100715,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/production/leftNavBar.tpl' => 1,
    'file:template/production/topNavBar.tpl' => 1,
    'file:template/production/footerBar.tpl' => 1,
  ),
),false)) {
function content_5c73f8ff000e63_13451162 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="template/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


        <!-- Custom styling plus plugins -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/leftNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/topNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-sm-4">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=commande">Commandes</a></li>
                                            <li class="active"><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre-->  
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Listes des Commandes</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <form class="pos-ajout" method="POST" action="index.php">
                                            <input type="hidden" name="gestion" value="commande">
                                            <input type="hidden" name="action" value="form_ajouter">
                                            <label>Passer une Commande : <input id="aImage" type="image" name="btn_ajouter" src='template/images/icones/a16.png'></label>
                                        </form>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <table id="datatable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Numéros</th>
                                                <th>Vendeur</th>
                                                <th>Client</th>
                                                <th>Montant HT en €</th>
                                                <th class="pos-actions">Consulter</th>
                                                <th class="pos-actions">Modifier</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
$_from = $_smarty_tpl->tpl_vars['listeCommande']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_commande_0_saved_item = isset($_smarty_tpl->tpl_vars['commande']) ? $_smarty_tpl->tpl_vars['commande'] : false;
$_smarty_tpl->tpl_vars['commande'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['commande']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['commande']->value) {
$_smarty_tpl->tpl_vars['commande']->_loop = true;
$__foreach_commande_0_saved_local_item = $_smarty_tpl->tpl_vars['commande'];
?>

                                                <tr>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['commande']->value['numero'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['commande']->value['login'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['commande']->value['nom'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['commande']->value['total_ht'];?>
</td>
                                                    <td class="pos-actions">
                                                        <form method="POST" action="index.php">
                                                            <input type="hidden" name="gestion" value="commande">
                                                            <input type="hidden" name="action" value="form_consulter">
                                                            <input type="hidden" name="f_numero" value="<?php echo $_smarty_tpl->tpl_vars['commande']->value['numero'];?>
">
                                                            <input id="pImage" type="image" name="btn_consulter" src='template/images/icones/p16.png'>
                                                        </form>
                                                    </td>
                                                    <!--FAUT UN IF ICI-->
                                                    <?php if ($_smarty_tpl->tpl_vars['commande']->value['valide'] == 0) {?>
                                                        <td class="pos-actions">
                                                            <form method="POST" action="index.php">
                                                                <input type="hidden" name="gestion" value="commande">
                                                                <input type="hidden" name="action" value="form_modifier">
                                                                <input type="hidden" name="f_numero" value="<?php echo $_smarty_tpl->tpl_vars['commande']->value['numero'];?>
">
                                                                <input id="mImage" type="image" name="btn_modifier" src='template/images/icones/m16.png'>
                                                            </form>
                                                        </td>
                                                        <?php } else { ?>
                                                        <td class="pos-actions">
                                                            <p>Validée</p>
                                                        </td>
                                                    <?php }?>

                                                    <!--FAUT UN IF ICI-->
                                                </tr>
                                            <?php
$_smarty_tpl->tpl_vars['commande'] = $__foreach_commande_0_saved_local_item;
}
if ($__foreach_commande_0_saved_item) {
$_smarty_tpl->tpl_vars['commande'] = $__foreach_commande_0_saved_item;
}
?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
                <!-- /page content -->

                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/footerBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            </div>
        </div>

        <!-- jQuery -->
        <?php echo '<script'; ?>
 src="template/vendors/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
        <!-- Bootstrap -->
        <?php echo '<script'; ?>
 src="template/vendors/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        <!-- FastClick -->
        <?php echo '<script'; ?>
 src="template/vendors/fastclick/lib/fastclick.js"><?php echo '</script'; ?>
>
        <!-- NProgress -->
        <?php echo '<script'; ?>
 src="template/vendors/nprogress/nprogress.js"><?php echo '</script'; ?>
>
        <!-- iCheck -->
        <?php echo '<script'; ?>
 src="../../vendors/iCheck/icheck.min.js"><?php echo '</script'; ?>
>
        <!-- Datatables -->
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.flash.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.html5.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-buttons/js/buttons.print.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/jszip/dist/jszip.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/pdfmake/build/pdfmake.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/vendors/pdfmake/build/vfs_fonts.js"><?php echo '</script'; ?>
>


        <!-- Custom Theme Scripts -->
        <?php echo '<script'; ?>
 src="template/build/js/custom.min.js"><?php echo '</script'; ?>
>
    </body>
</html><?php }
}
