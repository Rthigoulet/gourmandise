<?php
/* Smarty version 3.1.29, created on 2019-03-01 14:55:52
  from "C:\xampp\htdocs\gourmandise\mod_client\vue\clientFicheVue.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c7939e8477a10_48459721',
  'file_dependency' => 
  array (
    '4208a1fe34799e104e0060864d29fe2544a498a9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_client\\vue\\clientFicheVue.tpl',
      1 => 1551448550,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/production/leftNavBar.tpl' => 1,
    'file:template/production/topNavBar.tpl' => 1,
    'file:mod_client/vue/clientStatVue.tpl' => 1,
    'file:template/production/footerBar.tpl' => 1,
  ),
),false)) {
function content_5c7939e8477a10_48459721 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">

        <div class="container body">
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/leftNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <div class="main_container">

                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/topNavBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


                <div class="right_col" role="main">
                    <div class="">

                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-sm-4">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=client">Clients</a></li>
                                            <li class="active"><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre-->        

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2><?php echo $_smarty_tpl->tpl_vars['titreForm']->value;?>
</h2>
                                        <a class="pull-right"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <br />
                                        <form action="index.php" method="POST" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                            <input type="hidden" name="gestion"  value="client" >
                                            <input type="hidden" name="action"  value="<?php echo $_smarty_tpl->tpl_vars['valAction']->value;?>
" >

                                            <?php if ($_smarty_tpl->tpl_vars['valAction']->value != 'ajouter') {?>
                                                <div class="form-group">
                                                    <label for="code_c" class="control-label col-md-3 col-sm-3 col-xs-12">Code Client : </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="f_code_c"  class="form-control" readonly="readonly" value="<?php echo $_smarty_tpl->tpl_vars['code_c']->value;?>
">
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <input type="hidden" name="f_code_c"  class="form-control" readonly="readonly" value="<?php echo $_smarty_tpl->tpl_vars['code_c']->value;?>
">
                                            <?php }?>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nom">Nom & Prénom <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="f_nom" required="required" value="<?php echo $_smarty_tpl->tpl_vars['nom']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['readonlyON']->value;?>
 class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="adresse">Adresse <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="f_adresse"  required="required" value="<?php echo $_smarty_tpl->tpl_vars['adresse']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['readonlyON']->value;?>
 class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="cp" class="control-label col-md-3 col-sm-3 col-xs-12">CP <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_cp" class="form-control col-md-7 col-xs-12" value="<?php echo $_smarty_tpl->tpl_vars['cp']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['readonlyON']->value;?>
 type="text" required="required" name="middle-name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="ville" class="control-label col-md-3 col-sm-3 col-xs-12">Ville <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_ville" class="text-uppercase form-control col-md-7 col-xs-12" value="<?php echo $_smarty_tpl->tpl_vars['ville']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['readonlyON']->value;?>
 type="text" required="required" name="middle-name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="telephone" class="control-label col-md-3 col-sm-3 col-xs-12">Téléphone <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_telephone" class="form-control col-md-7 col-xs-12" value="<?php echo $_smarty_tpl->tpl_vars['telephone']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['readonlyON']->value;?>
 required="required" type="text">
                                                </div>
                                            </div>
                                            <div class="ln_solid"></div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                    <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=client"'>
                                                    <button class="btn btn-primary" type="reset">Reset</button>
                                                    <?php if ($_smarty_tpl->tpl_vars['valBtnAction']->value != '') {?><input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="<?php echo $_smarty_tpl->tpl_vars['valBtnAction']->value;?>
" ><?php }?>
                                                    <button class="btn btn-default pull-right" onclick="window.print();"><i class="fa fa-print"></i> Imprimer</button>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['valAction']->value != 'ajouter') {?>
                        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:mod_client/vue/clientStatVue.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
            </div>
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:template/production/footerBar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <!-- jQuery -->
            <?php echo '<script'; ?>
 src="template/vendors/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
            <!-- Bootstrap -->
            <?php echo '<script'; ?>
 src="template/vendors/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
            <!-- FastClick -->
            <?php echo '<script'; ?>
 src="template/vendors/fastclick/lib/fastclick.js"><?php echo '</script'; ?>
>
            <!-- NProgress -->
            <?php echo '<script'; ?>
 src="template/vendors/nprogress/nprogress.js"><?php echo '</script'; ?>
>
            <!-- Chart.js -->
            <?php echo '<script'; ?>
 src="template/vendors/Chart.js/dist/Chart.min.js"><?php echo '</script'; ?>
>
            <!-- gauge.js -->
            <?php echo '<script'; ?>
 src="template/vendors/gauge.js/dist/gauge.min.js"><?php echo '</script'; ?>
>
            <!-- bootstrap-progressbar -->
            <?php echo '<script'; ?>
 src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"><?php echo '</script'; ?>
>
            <!-- iCheck -->
            <?php echo '<script'; ?>
 src="template/vendors/iCheck/icheck.min.js"><?php echo '</script'; ?>
>
            <!-- Skycons -->
            <?php echo '<script'; ?>
 src="template/vendors/skycons/skycons.js"><?php echo '</script'; ?>
>
            <!-- Flot -->
            <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.pie.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.time.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.stack.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/Flot/jquery.flot.resize.js"><?php echo '</script'; ?>
>
            <!-- Flot plugins -->
            <?php echo '<script'; ?>
 src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/flot.curvedlines/curvedLines.js"><?php echo '</script'; ?>
>
            <!-- DateJS -->
            <?php echo '<script'; ?>
 src="template/vendors/DateJS/build/date.js"><?php echo '</script'; ?>
>
            <!-- JQVMap -->
            <?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/jquery.vmap.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"><?php echo '</script'; ?>
>
            <!-- bootstrap-daterangepicker -->
            <?php echo '<script'; ?>
 src="template/vendors/moment/min/moment.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"><?php echo '</script'; ?>
>

            <!-- Custom Theme Scripts -->
            <?php echo '<script'; ?>
 src="template/build/js/custom.min.js"><?php echo '</script'; ?>
>

    </body>
</html><?php }
}
