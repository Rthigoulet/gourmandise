<?php

define('GESTION', $gestion);
require_once('mod_' . $gestion . '/modele/' . $gestion . 'Modele.php');

//Traitement de la liste
function vueParDefaut($message = null) {
    $idRequete = liste();
    // Chargement de la liste par défaut
    require_once('mod_' . GESTION . '/vue/' . GESTION . 'Vue.php');
}

//Traitement du chargement des fiches
function form_action($parametre, $message = null) {

    switch ($parametre['action']) {
        case 'form_consulter' :
        case 'form_modifier' :
        case 'form_supprimer' :
        case 'modifier' :
        case 'supprimer' :
            //Partie statistique à traiter 
            $idStat01 = stat01($parametre);
            $idStat02 = stat02($parametre);
            $idStat03 = stat03($parametre);
            $idRequete = fiche($parametre);
            break;
    }
    require_once('mod_' . GESTION . '/vue/' . GESTION . 'Vue.php');
}

//Traitement de l'action en écriture
function action($parametre, $message = "") {

    //Pour le contrôle
    switch ($parametre['action']) {

        case 'modifier' :
            $message = controleSaisie($parametre);
            break;

        case 'ajouter' :
            $message = controleSaisie($parametre);
            break;

        case 'supprimer' :
            $idRequete = controleSuppression($parametre);

            if ($idRequete->rowCount() > 0) {
                $message = "Suppression impossible.";
                form_action($parametre, $message);
            }
            break;
    }


    if (!empty($message)) {
        form_action($parametre, $message);
    } else {


        $t = $parametre['action'];
        //Traitement de l'action
        $idRequete = $t($parametre);

        if ($idRequete) {
            switch ($parametre['action']) {

                case 'modifier' :

                    $msg = "Modification effectuée avec succès !";
                    break;

                case 'ajouter' :
                    $msg = "Ajout effectué avec succès !";
                    break;

                case 'supprimer' :

                    $msg = "Suppression effectuée  avec succès !";
                    break;
            }
        } else {

            $msg = "Problème lors de l'execution de la dernière action";
        }

        vueParDefaut($msg);
    }
}

// Contrôle des saisies 
function controleSaisie($parametre) {

    $message = "";

    if (empty($parametre['f_designation'])) {
        $message .= "La saisie de la désignation est obligatoire. ";
    }
    if (empty($parametre['f_quantite'])) {
        $message .= "La saisie de la quantité est obligatoire. ";
    }
    if (empty($parametre['f_descriptif'])) {
        $message .= "La saisie du descriptif est obligatoire. ";
    }
    if (empty($parametre['f_prix_unitaire_HT'])) {
        $message .= "La saisie du Prix unitaire est obligatoire. ";
    }
    if (empty($parametre['f_stock'])) {
        $message .= "La saisie du stock est obligatoire. ";
    }
    if (empty($parametre['f_poids_piece'])) {
        $message .= "La saisie de Poids de la pièce est obligatoire. ";
    }

    return $message;
}
