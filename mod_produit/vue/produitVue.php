<?php

$tpl = new Smarty();

// Pour header.tpl
$tpl->assign('nomAffiche',$_SESSION['prenom']. ' ' .$_SESSION['nom']);
$tpl->assign('title','Gourmandise | Produits');

if (isset($parametre['action'])) {


    switch ($parametre['action']) {

        case 'form_consulter':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Produit : Consultation");
            $tpl->assign("readonlyON", "readonly='readonly'");
            $tpl->assign("valAction", "");
            $tpl->assign("valBtnAction", "");
            $tpl->assign("nbProduit", "0");
            $tpl->assign("message", $message);
            chargementFiche($tpl, $idRequete);
            chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);

            break;

        case 'form_supprimer':
        case 'supprimer':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Produit : Suppression");
            $tpl->assign("readonlyON", "readonly='readonly'");
            $tpl->assign("valAction", "supprimer");
            $tpl->assign("valBtnAction", "Supprimer");
            $tpl->assign("nbProduit", "0");
            $tpl->assign("message", $message);
            chargementFiche($tpl, $idRequete);
            chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);

            break;



        case 'form_modifier':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Produit : Modification");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "modifier");
            $tpl->assign("valBtnAction", "Modifier");
            $tpl->assign("message", $message);
            chargementFiche($tpl, $idRequete);
            chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);

            break;

        case 'form_ajouter':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Produit : Création");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "ajouter");
            $tpl->assign("valBtnAction", "Ajouter");
            $tpl->assign("ca", "");
            $tpl->assign("listeProduits", "");
            $tpl->assign("partCa", "");
            $tpl->assign("message", $message);
            chargementFicheVierge($tpl);
            chargementMessageErreur($tpl, $message);
            break;

        case 'modifier':
            // Cas uniquement en retour de modification !
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Produit : Modification");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "modifier");
            $tpl->assign("valBtnAction", "Modifier");
            chargementFicheRetourAction($tpl, $parametre);
            chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);

            break;


        case 'ajouter':
            // Cas uniquement en retour d'ajout !
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Produit : Création");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "ajouter");
            $tpl->assign("valBtnAction", "Ajouter");
            chargementFicheRetourAction($tpl, $parametre);
            $tpl->assign("ca", "");
            $tpl->assign("listeProduits", "");
            $tpl->assign("partCa", "");
            chargementMessageErreur($tpl, $message);

            break;
    }

    //Affichage des fiches
    $tpl->display('mod_produit/vue/produitFicheVue.tpl');
} else {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alert">' . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }

    // Chargement des données produits
    $listeProduits = array();
    $i = 0;
    while ($row = $idRequete->fetch()) {
        $listeProduits[$i]['reference'] = $row['reference'];
        $listeProduits[$i]['designation'] = $row['designation'];
        $listeProduits[$i]['quantite'] = $row['quantite'];
        $listeProduits[$i]['descriptif'] = $row['descriptif'];
        $listeProduits[$i]['prix_unitaire_HT'] = $row['prix_unitaire_HT'];
        $listeProduits[$i]['stock'] = $row['stock'];
        $listeProduits[$i]['poids_piece'] = $row['poids_piece'];

        $i++;
    }
    // Pour corps de page
    $tpl->assign("titreForm", "Liste des Produits");
    $tpl->assign('listeProduits', $listeProduits);
    //Affichage des listes

    $tpl->display('mod_produit/vue/produitListeVue.tpl');
}

function chargementFiche($tpl, $idRequete) {

    $row = $idRequete->fetch();
    $tpl->assign("reference", $row['reference']);
    $tpl->assign("designation", $row['designation']);
    $tpl->assign("quantite", $row['quantite']);
    $tpl->assign("descriptif", $row['descriptif']);
    $tpl->assign("prix_unitaire_HT", $row['prix_unitaire_HT']);
    $tpl->assign("stock", $row['stock']);
    $tpl->assign("poids_piece", $row['poids_piece']);
}

function chargementFicheRetourAction($tpl, $parametre) {

    $tpl->assign("reference", $parametre['f_reference']);
    $tpl->assign("designation", $parametre['f_designation']);
    $tpl->assign("quantite", $parametre['f_quantite']);
    $tpl->assign("descriptif", $parametre['f_descriptif']);
    $tpl->assign("prix_unitaire_HT", $parametre['f_prix_unitaire_HT']);
    $tpl->assign("stock", $parametre['f_stock']);
    $tpl->assign("poids_piece", $parametre['f_poids_piece']);
}

function chargementFicheVierge($tpl) {

    $tpl->assign("reference", "");
    $tpl->assign("designation", "");
    $tpl->assign("quantite", "");
    $tpl->assign("descriptif", "");
    $tpl->assign("prix_unitaire_HT", "");
    $tpl->assign("stock", "");
    $tpl->assign("poids_piece", "");
}

function chargementMessageErreur($tpl, $message) {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alertErreur">'
                . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }
}

function chargementStatistique($tpl, $idStat01, $idStat03, $idStat02) {

    //Chargement chiffre d'affaire du client
    $row = $idStat01->fetch(PDO::FETCH_NUM);
    $tpl->assign("ca", $row[1] . ' €');


    // Chargement les 5 produits les plus achetés pour 1 client

    if ($idStat03->rowCount() == 0) {
        $tpl->assign('listeProduits', 0);
    } else {
        $listeProduits = array();
        $i = 0;
        while ($ligne = $idStat03->fetch(PDO::FETCH_NUM)) {
            $listeProduits[$i]['designation'] = $ligne[0];
            $listeProduits[$i]['nbProduits'] = $ligne[1];
            $i++;
        }
        $tpl->assign('listeProduits', $listeProduits);
    }

    //Chargement CA de l'entreprise Gourmandise SARL'
    $res = $idStat02->fetch(PDO::FETCH_NUM);
    $caGlobal = $res[0];
    //Calcul de la part de CA du client pour Gourmandise
    $partCaClient = ($row[1] * 100) / $caGlobal;

    //Assignation des zones
    $tpl->assign("partCa", $partCaClient);
}