<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title>{$title}</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">

        <div class="container body">
            {include 'template/production/leftNavBar.tpl'}
            <div class="main_container">

                {include 'template/production/topNavBar.tpl'}


                <div class="right_col" role="main">
                    <div class="">
                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-sm-4">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=produit">Produits</a></li>
                                            <li class="active">{$titreForm}</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre-->
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>{$titreForm}</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <br />
                                        <form action="index.php" method="POST" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                            <input type="hidden" name="gestion"  value="produit" >
                                            <input type="hidden" name="action"  value="{$valAction}" >

                                            {if $valAction != 'ajouter'}
                                                <div class="form-group">
                                                    <label for="reference" class="control-label col-md-3 col-sm-3 col-xs-12">Reference Produit : </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="f_reference"  class="form-control" readonly="readonly" value="{$reference}">
                                                    </div>
                                                </div>
                                            {else}
                                                <input type="hidden" name="f_reference"  class="form-control" readonly="readonly" value="{$reference}">
                                            {/if}

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="designation">Désignation <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="f_designation" required="required" value="{$designation}" {$readonlyON} class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quantite">Quantité <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="numeric" name="f_quantite"  required="required" placeholder="Poids du produit ou nombre de pièces" value="{$quantite}" {$readonlyON} class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="descriptif" class="control-label col-md-3 col-sm-3 col-xs-12">Descriptif <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_descriptif" placeholder="Unité de mesure G pour gramme et P pour Pièce" class="text-uppercase form-control col-md-7 col-xs-12" value="{$descriptif}" {$readonlyON} type="text" required="required" name="middle-name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="prix_unitaire_HT" class="control-label col-md-3 col-sm-3 col-xs-12">Prix Unitaire HT <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_prix_unitaire_HT" class="form-control col-md-7 col-xs-12" value="{$prix_unitaire_HT}" {$readonlyON} type="numeric" required="required" name="middle-name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="stock" class="control-label col-md-3 col-sm-3 col-xs-12">Stock <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_stock" class="date-picker form-control col-md-7 col-xs-12" value="{$stock}" {$readonlyON} required="required" type="numeric">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="poids_piece" class="control-label col-md-3 col-sm-3 col-xs-12">Poids Pièce <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_poids_piece" placeholder="En grammes pour les articles vendus par pièce" class="form-control col-md-7 col-xs-12" value="{$poids_piece}" {$readonlyON} required="required" type="numeric">
                                                </div>
                                            </div>
                                            <div class="ln_solid"></div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                    <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=produit"'>
                                                    <button class="btn btn-primary" type="reset">Reset</button>
                                                    {if $valBtnAction != ""}<input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="{$valBtnAction}" >{/if}
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {if $valAction != 'ajouter'}
                        {include file='mod_produit/vue/produitStatVue.tpl'}
                    {/if}
                </div>
            </div>
        </div>
        {include 'template/production/footerBar.tpl'}
        <!-- jQuery -->
        <script src="template/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="template/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="template/vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="template/vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="template/vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="template/vendors/iCheck/icheck.min.js"></script>
        <!-- Skycons -->
        <script src="template/vendors/skycons/skycons.js"></script>
        <!-- Flot -->
        <script src="template/vendors/Flot/jquery.flot.js"></script>
        <script src="template/vendors/Flot/jquery.flot.pie.js"></script>
        <script src="template/vendors/Flot/jquery.flot.time.js"></script>
        <script src="template/vendors/Flot/jquery.flot.stack.js"></script>
        <script src="template/vendors/Flot/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="template/vendors/flot.curvedlines/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="template/vendors/DateJS/build/date.js"></script>
        <!-- JQVMap -->
        <script src="template/vendors/jqvmap/dist/jquery.vmap.js"></script>
        <script src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="template/vendors/moment/min/moment.min.js"></script>
        <script src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="template/build/js/custom.min.js"></script>

    </body>
</html>
