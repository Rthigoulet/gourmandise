<?php

function liste() {
    $cnx = getBdd();
    $sql = "SELECT * FROM produit ";
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}


function fiche($parametre){
    $cnx = getBdd();
    $sql = "SELECT * FROM produit WHERE reference = ?";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_reference']));
    return $idRequete;
}

function modifier($parametre){
    $cnx = getBdd();
    $sql = "UPDATE produit SET designation = ?, quantite = ?, descriptif = ?, prix_unitaire_HT = ?,"
            . " stock= ?, poids_piece = ? WHERE reference = ?";
    $idRequete = executeRequete($cnx, $sql,
            array($parametre['f_designation'],$parametre['f_quantite'],
                $parametre['f_descriptif'],$parametre['f_prix_unitaire_HT'],
                $parametre['f_stock'],$parametre['f_poids_piece'],$parametre['f_reference']));
    return $idRequete;
}

function ajouter($parametre){
    
    $cnx = getBdd();
    
    $id = intval($parametre['f_reference']);
   
    $sql = "INSERT INTO produit VALUES(?,?,?,?,?,?,?)";
    $idRequete = executeRequete($cnx, $sql,
            array($id,$parametre['f_designation'],$parametre['f_quantite'],
                $parametre['f_descriptif'],$parametre['f_prix_unitaire_HT'],
                $parametre['f_stock'],$parametre['f_poids_piece']));
    return $idRequete;  
}

function supprimer($parametre){
    
    $cnx = getBdd();

    $sql = "DELETE FROM produit WHERE reference = ?";
    $idRequete = executeRequete($cnx, $sql,
            array($parametre['f_reference']));
    
    return $idRequete;
}


/***** CONTROLE SUPPRESSION  *****/
function controleSuppression($parametre){
    $cnx = getBdd();

    $sql = "SELECT * FROM ligne_commande WHERE reference = ?";
    $idRequete = executeRequete($cnx, $sql,
            array($parametre['f_reference']));
    
    return $idRequete; 
}

/***** RECHERCHES COMPLEMENTAIRES *****/

function stat01($parametre){
    //CA du client
    $cnx = getBdd();
    $sql = "SELECT code_c, SUM(total_ht) "
            . "FROM commande WHERE code_c = ? GROUP BY code_c";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_reference']));
    return $idRequete;
}

function stat02($parametre){
    //Pour un calcul en % du CA réalisé
    $cnx = getBdd();
    $sql = "SELECT SUM(total_ht) FROM commande";
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}


function stat03($parametre){
    // 5 articles favoris du client
    $cnx = getBdd();
    $sql = "SELECT designation, 
        COUNT(ligne_commande.reference * quantite_demandee)
        FROM commande, ligne_commande, produit
        WHERE commande.numero = ligne_commande.numero
        AND ligne_commande.reference = produit.reference
        AND code_c = ?
        GROUP BY designation
        ORDER BY 2 DESC
        LIMIT 5";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_reference']));
    return $idRequete;
}
