<?php

require_once('mod_' . $gestion . '/controleur/' . $gestion . 'Controleur.php');


if (isset($_REQUEST['action'])) {

    switch ($_REQUEST['action']) {
        case 'authentification':
            //L'utilisateur cherche à s'authentifier
            authentification($_REQUEST);
            break;
        
        case 'deconnexion':
            //L'utilisateur demande la deconnexion
            deconnexion($_REQUEST);
            break;
        
        case 'form_consulter' :
        case 'form_modifier' :
        case 'form_supprimer' :
        case 'form_ajouter' :
            // Rechercher l'enregistrement
            form_action($_REQUEST);
            break;
        
        case 'modifier' :
        case 'ajouter' :
        case 'supprimer' :
            action($_REQUEST);
            break;
        default:
            echo "IMPOSSIBLE DE PASSER ICI !!!";
            break;
    }
    
} else {

    vueParDefaut();
}