<?php

define('GESTION', $gestion);
require_once('mod_' . $gestion . '/modele/' . $gestion . 'Modele.php');

//Traitement du chargement des fiches
function form_action($parametre, $message = null) {

    switch ($parametre['action']) {


        case 'form_modifier' :
        case 'modifier' :
        case 'supprimer' :
            //Partie statistique à traiter            
            $idRequete = fiche($parametre);
            break;
    }

    require_once('mod_' . GESTION . '/vue/' . GESTION . 'Vue.php');
}

function action($parametre, $message = "") {

    //Pour le contrôle
    switch ($parametre['action']) {

        case 'modifier' :
            $message = controleSaisie($parametre);
            break;
    }


    if (!empty($message)) {

        form_action($parametre, $message);
    } else {


        $t = $parametre['action'];
        //Traitement de l'action
        $idRequete = $t($parametre);

        if ($idRequete) {

            $message = "Modification effectuée avec succès !";

            $parametre['action'] = 'form_modifier';
            form_action($parametre, $message);
        } else {

            $message = "Problème lors de l'execution de la dernière action";
            form_action($parametre, $message);
        }
    }
}

// Contrôle des saisies 
function controleSaisie($parametre) {

    $message = "";

    if (empty($parametre['f_adresse'])) {
        $message .= "La saisie de l'adresse est obligatoire. ";
    }
    if (empty($parametre['f_cp'])) {
        $message .= "La saisie du Code Postal est obligatoire. ";
    }
    if (empty($parametre['f_ville'])) {
        $message .= "La saisie de la ville est obligatoire. ";
    }
    if (empty($parametre['f_telephone'])) {
        $message .= "La saisie du téléphone est obligatoire. ";
    }
    if (!empty($parametre['f_motdepasse'])) {
        if ($parametre['f_motdepasse_verif'] != $parametre['f_motdepasse']) {
            $message .= "Le mot de passe doit etre identique ";
        }
    }
    return $message;
}
