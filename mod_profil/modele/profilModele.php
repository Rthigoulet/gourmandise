<?php

function liste() {

    $cnx = getBdd();
    $sql = "SELECT * FROM vendeur ";
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}

function fiche($parametre) {

    $cnx = getBdd();
    $sql = "SELECT * FROM vendeur WHERE code_v = ?";

    $idRequete = executeRequete($cnx, $sql, array($_SESSION['code_v']));
    return $idRequete;
}

function modifier($parametre) {

    $cnx = getBdd();

    if (!empty($parametre['f_motdepasse_verif'])) {
        $temp = $parametre['f_motdepasse_verif'];
        /* Cryptage & décryptage */
        $gauche = 'ar30&y%';
        $droite = 'tk!@';
        $jeton = hash('ripemd128', "$gauche$temp$droite");
        $sql = "UPDATE vendeur SET nom = ?, prenom = ?, adresse = ?, cp = ?,"
                . " ville= ?, telephone = ?, motdepasse = ? WHERE code_v = ?";
        $idRequete = executeRequete($cnx, $sql, array($parametre['f_nom'], $parametre['f_prenom'],
            $parametre['f_adresse'], $parametre['f_cp'],
            $parametre['f_ville'], $parametre['f_telephone'],
            $jeton, $parametre['f_code_v']));
    } else {
        $sql = "UPDATE vendeur SET nom = ?, prenom = ?, adresse = ?, cp = ?,"
                . " ville= ?, telephone = ? WHERE code_v = ?";
        $idRequete = executeRequete($cnx, $sql, array($parametre['f_nom'], $parametre['f_prenom'],
            $parametre['f_adresse'], $parametre['f_cp'],
            $parametre['f_ville'], $parametre['f_telephone'],
            $parametre['f_code_v']));
    }

    return $idRequete;
}
