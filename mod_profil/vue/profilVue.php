<?php

$tpl = new Smarty();

// Pour header.tpl
$tpl->assign('nomAffiche',$_SESSION['prenom']. ' ' .$_SESSION['nom']);
$tpl->assign('title','Gourmandise | Profil');


if (isset($parametre['action'])) {

    switch ($parametre['action']) {

        case 'form_modifier':
            // Pour corps de page
            $tpl->assign("titreForm", "Profil : Modification");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "modifier");
            $tpl->assign("valBtnAction", "Modifier");
            chargementFiche($tpl, $idRequete);
//          chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessage($tpl, $message);
            break;
     
        case 'modifier':
            // Cas uniquement en retour de modification !
            // Pour corps de page
            $tpl->assign("titreForm", "Profil: Modification");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "modifier");
            $tpl->assign("valBtnAction", "Modifier");
            chargementFicheRetourAction($tpl, $parametre);
//          chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);
            break;
        
    }

    //Affichage des fiches
    $tpl->display('mod_profil/vue/profilFicheVue.tpl');
}

function chargementFiche($tpl, $idRequete) {

    $row = $idRequete->fetch();
    $tpl->assign("code_v", $row['code_v']);
    $tpl->assign("nom", $row['nom']);
    $tpl->assign("prenom", $row['prenom']);
    $tpl->assign("adresse", $row['adresse']);
    $tpl->assign("cp", $row['cp']);
    $tpl->assign("ville", $row['ville']);
    $tpl->assign("telephone", $row['telephone']);
    $tpl->assign("motdepasse", '');
}

function chargementFicheRetourAction($tpl, $parametre) {

    $tpl->assign("code_v", $parametre['f_code_v']);
    $tpl->assign("nom", $parametre['f_nom']);
    $tpl->assign("prenom", $parametre['f_prenom']);
    $tpl->assign("adresse", $parametre['f_adresse']);
    $tpl->assign("cp", $parametre['f_cp']);
    $tpl->assign("ville", $parametre['f_ville']);
    $tpl->assign("telephone", $parametre['f_telephone']);
    $tpl->assign("motdepasse", '');
}


function chargementMessageErreur($tpl, $message) {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alertErreur">'
                . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }
}

function chargementMessage($tpl, $message) {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alert">'
                . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }
}