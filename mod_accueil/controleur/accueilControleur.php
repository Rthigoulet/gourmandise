<?php

define('GESTION', $gestion);
require_once('mod_' . $gestion . '/modele/' . $gestion . 'Modele.php');

function vueParDefaut($parametre = null) {
    
/****************************************  Le CA du vendeur ****************************************/
    $votreCA = votreCA($parametre);
/****************************************  Le CA du vendeur ****************************************/    

/****************************************  Nombres Clients ****************************************/
    $nbClients2017 = nbClientTotal2017($parametre);
    $nbClients2018 = nbClientTotal2018($parametre);
    $nbClients2019 = nbClientTotal2019($parametre);
/****************************************  Nombres Clients ****************************************/
    
/****************************************  Chiffre Affaires  ****************************************/
    $cAffairesTotal2017 = chiffreAffaireTotal2017($parametre);
    $cAffairesTotal2018 = chiffreAffaireTotal2018($parametre);
    $cAffairesTotal2019 = chiffreAffaireTotal2019($parametre);
/****************************************  Chiffre Affaires  ****************************************/
    
    
/****************************************  Moyenne Commande  ****************************************/
    $moyenneCommande2017 = moyenneCommande2017($parametre);
    $moyenneCommande2018 = moyenneCommande2018($parametre);
    $moyenneCommande2019 = moyenneCommande2019($parametre);
/****************************************  Moyenne Commande  ****************************************/    
    
    require_once 'mod_accueil/vue/accueilVue.php';
}
