<?php


$tpl = new Smarty();
$tpl->assign('title','Gourmandise | Accueil');
$tpl->assign('nomAffiche',$_SESSION['prenom']. ' ' .$_SESSION['nom']);


/****************************************  Le CA du vendeur ****************************************/

chargementvotreCA($tpl, $votreCA);

/****************************************  Le CA du vendeur ****************************************/

/****************************************  Nombres Clients ****************************************/
chargementNbClientsTotal2017($tpl, $nbClients2017);
chargementNbClientsTotal2018($tpl, $nbClients2018);
chargementNbClientsTotal2019($tpl, $nbClients2019);
/****************************************  Nombres Clients ****************************************/

/****************************************  Chiffre Affaires  ****************************************/
chargementCAffairesTotal2017($tpl, $cAffairesTotal2017);
chargementCAffairesTotal2018($tpl, $cAffairesTotal2018);
chargementCAffairesTotal2019($tpl, $cAffairesTotal2019);
/****************************************  Chiffre Affaires  ****************************************/

/****************************************  Moyenne Commande ****************************************/
chargementMoyenneCommande2017($tpl, $moyenneCommande2017);
chargementMoyenneCommande2018($tpl, $moyenneCommande2018);
chargementMoyenneCommande2019($tpl, $moyenneCommande2019);
/****************************************  Moyenne Commande  ****************************************/


$tpl->display('mod_accueil/vue/accueilVue.tpl');


/****************************************  Le CA du vendeur ****************************************/
function chargementvotreCA($tpl, $votreCA){
    
    $res = $votreCA->fetch(PDO::FETCH_NUM);
    $CA = $res[0];
    if($CA == 0){
        $tpl->assign('votreCA',0);
    }else{
        $tpl->assign('votreCA',$CA);
}
}
/****************************************  Le CA du vendeur ****************************************/

/****************************************  Nombres Clients ****************************************/
function chargementNbClientsTotal2017($tpl, $nbClients2017){
    
    $res = $nbClients2017->fetch(PDO::FETCH_NUM);
    $clients2017 = $res[0];
    if($clients2017 == 0){
        $tpl->assign('nbClientsTotal2017',0);
    }else{
    $tpl->assign('nbClientsTotal2017',$clients2017);
    }
}
function chargementNbClientsTotal2018($tpl, $nbClients2018){
    
    $res = $nbClients2018->fetch(PDO::FETCH_NUM);
    $clients2018 = $res[0];
        if($clients2018 == 0){
        $tpl->assign('nbClientsTotal2018',0);
    }else{
    $tpl->assign('nbClientsTotal2018',$clients2018);
    }
}
function chargementNbClientsTotal2019($tpl, $nbClients2019){
    
    $res = $nbClients2019->fetch(PDO::FETCH_NUM);
    $clients2019 = $res[0];
        if($clients2019 == 0){
        $tpl->assign('nbClientsTotal2019',0);
    }else{
    $tpl->assign('nbClientsTotal2019',$clients2019);
    }
}
/****************************************  Nombres Clients ****************************************/


/****************************************  Chiffre Affaires  ****************************************/
function chargementCAffairesTotal2017($tpl, $cAffairesTotal2017){
    
    $res = $cAffairesTotal2017->fetch(PDO::FETCH_NUM);
    $cAffaires2017 = $res[0];
        if($cAffaires2017 == 0){
        $tpl->assign('chiffresAffaires2017', 0);
    }else{
    $tpl->assign('chiffresAffaires2017',$cAffaires2017);
    }
}
function chargementCAffairesTotal2018($tpl, $cAffairesTotal2018){
    
    $res = $cAffairesTotal2018->fetch(PDO::FETCH_NUM);
    $cAffaires2018 = $res[0];
        if($cAffaires2018 == 0){
        $tpl->assign('chiffresAffaires2018', 0);
    }else{
    $tpl->assign('chiffresAffaires2018',$cAffaires2018);
    }
}
function chargementCAffairesTotal2019($tpl, $cAffairesTotal2019){
    
    $res = $cAffairesTotal2019->fetch(PDO::FETCH_NUM);
    $cAffaires2019 = $res[0];
    if($cAffaires2019 == 0){
        $tpl->assign('chiffresAffaires2019', 0);
    }else{
        $tpl->assign('chiffresAffaires2019',$cAffaires2019);
}
}
/****************************************  Chiffre Affaires  ****************************************/

/****************************************  Moyenne Commande ****************************************/
function chargementMoyenneCommande2017($tpl, $moyenneCommande2017){
    
    $res = $moyenneCommande2017->fetch(PDO::FETCH_NUM);
    $mCommande2017 = $res[0];
    if($mCommande2017 == 0){
        $tpl->assign('moyenneCommande2017',0);
    }else{
    $tpl->assign('moyenneCommande2017',$mCommande2017);
    }
}
function chargementMoyenneCommande2018($tpl, $moyenneCommande2018){
    
    $res = $moyenneCommande2018->fetch(PDO::FETCH_NUM);
    $mCommande2018 = $res[0];
    if($mCommande2018 == 0){
        $tpl->assign('moyenneCommande2018',0);
    }else{
    $tpl->assign('moyenneCommande2018',$mCommande2018);
    }
}
function chargementMoyenneCommande2019($tpl, $moyenneCommande2019){
    
    $res = $moyenneCommande2019->fetch(PDO::FETCH_NUM);
    $mCommande2019 = $res[0];
    if($mCommande2019 == 0){
        $tpl->assign('moyenneCommande2019',0);
    }else{
    $tpl->assign('moyenneCommande2019',$mCommande2019);
    }
}
/****************************************  Moyenne Commande  ****************************************/