<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title>{$title}</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md footer">
        <div class="container body">
            <div class="main_container">
                {include 'template/production/topNavBar.tpl'}
                {include 'template/production/leftNavBar.tpl'}
                <div class="right_col" role="main">
                    <div class="">
                        <!--Titre-->
                        {*<div class="breadcrumbs">
                        <div class="col-sm-4">
                        <div class="page-header float-left">
                        <div class="page-title">
                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                        </div>
                        </div>
                        </div>
                        <div class="col-sm-8">
                        <div class="page-header float-right">
                        <div class="page-title">

                        </div>
                        </div>
                        </div>
                        </div>*}
                        <!--Titre-->
                        <!--Contenu 1.0 -->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h3>Bonjour 
                                        <a href="index.php?gestion=profil&action=form_modifier"><strong>{$nomAffiche}</strong></a>
                                        votre Chiffre d'affaires Global est de
                                        <strong>{$votreCA}</strong> € HT 
                                        <a class="close-link pull-right"><i class="fa fa-close"></i></a>
                                    </h3> 
                                </div>                 
                            </div>
                        </div>
                        <!--Contenu 1.0 -->
                        <!--Contenu 1.1 -->
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>L'année 2017 en Chiffres !</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_panel">
                                    <h4> <a class='fa fa-users' href="index.php?gestion=client"></a>  Le nombre de Clients</h4>
                                    <h3>{$nbClientsTotal2017}</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_panel">
                                    <h4><i class='fa fa-euro'></i>  Le chiffre d'affaires Global</h4>
                                    <h3>{$chiffresAffaires2017} € HT</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_panel">
                                    <h4><a class='fa fa-cubes' href="index.php?gestion=produit"></a>  Moyenne de produits par Commande</h4>
                                    <h3>{$moyenneCommande2017} Unité</h3>
                                </div>
                            </div>
                        </div>
                        <!--Contenu 1.1 -->

                        <!--Contenu 2-->
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>L'année 2018 en Chiffres !</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_panel">
                                    <h4> <a class='fa fa-users' href="index.php?gestion=client"></a>  Le nombre de Clients</h4>
                                    <h3>{$nbClientsTotal2018}</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_panel">
                                    <h4><i class='fa fa-euro'></i>  Le chiffre d'affaires Global</h4>
                                    <h3>{$chiffresAffaires2018} € HT</h3>
                                </div>
                                <div class="x_panel">
                                    <h4><a class='fa fa-cubes' href="index.php?gestion=produit"></a>  Moyenne de produits par Commande</h4>
                                    <h3>{$moyenneCommande2018} Unité</h3>
                                </div>
                            </div>
                        </div>
                        <!--Contenu 2-->
                        <!--Contenu 3-->
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>L'année 2019 en Chiffres !</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_panel">
                                    <h4> <a class='fa fa-users' href="index.php?gestion=client"></a>  Le nombre de Clients</h4>
                                    <h3>{$nbClientsTotal2019}</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_panel">
                                    <h4><i class='fa fa-euro'></i>  Le chiffre d'affaires Global</h4>
                                    <h3>{$chiffresAffaires2019} € HT</h3>
                                </div>
                                <div class="x_panel">
                                    <h4><a class='fa fa-cubes' href="index.php?gestion=produit"></a>  Moyenne de produits par Commande</h4>
                                    <h3>{$moyenneCommande2019} Unité</h3>
                                </div>
                            </div>
                        </div>
                        <!--Contenu 3-->

                        <!--Contenu 4 | CANVAS-->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel tile overflow_hidden">
                                <div class="x_title">
                                    <h2>Les Produits en vedette</h2>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table style="width:100%">
                                        <tr>
                                            <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <p>Top 8</p>
                                            </th>
                                            <th>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <p>Produits</p>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <canvas class="canvasDoughnut" height="270" width="270" style="margin: 15px 10px 10px 0"></canvas>
                                            </td>
                                            <td>
                                                <table class="tile_info">
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square blue"></i>IOS </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square green"></i>Android </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square aero"></i>Symbian </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p><i class="fa fa-square red"></i>Others </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--Contenu 4 | CANVAS-->


                        <!--Contenu 5-->
                        <!-- bar chart -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Le chiffre d'affaires des 8 meilleurs clients</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div id="graph_bar" style="width:100%; height:280px;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /bar charts -->
                    </div>

                </div>
            </div>
        </div>
        <!--Contenu 5-->
    </div>
</div>
</div>
</div>
<br>
<br>
</body>
{include 'template/production/footerBar.tpl'}

<!-- jQuery -->
<script src="template/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="template/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="template/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="template/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="template/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="template/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="template/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="template/vendors/Flot/jquery.flot.js"></script>
<script src="template/vendors/Flot/jquery.flot.pie.js"></script>
<script src="template/vendors/Flot/jquery.flot.time.js"></script>
<script src="template/vendors/Flot/jquery.flot.stack.js"></script>
<script src="template/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="template/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="template/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="template/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="template/vendors/moment/min/moment.min.js"></script>
<script src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- morris.js -->
<script src="template/vendors/raphael/raphael.min.js"></script>
<script src="template/vendors/morris.js/morris.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="template/build/js/custom.min.js"></script>
</html>
