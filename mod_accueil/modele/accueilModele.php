<?php
/****************************************  Le CA du vendeur ****************************************/
function votreCA(){
    $cnx = getBdd();
    $code_vCA = $_SESSION['code_v'];
    $sql = "SELECT SUM(commande.total_ht) FROM commande , vendeur WHERE vendeur.code_v = commande.code_v AND commande.valide = 1  AND vendeur.code_v = ". $code_vCA ." ";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
/****************************************  Le CA du vendeur ****************************************/

/****************************************  Nombres Clients ****************************************/
function nbClientTotal2017() {
    //nb clients total
    $cnx = getBdd();
    $sql = "SELECT COUNT(client.code_c) FROM client, commande WHERE client.code_c = commande.code_c AND date_commande LIKE '%2017%' ";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
function nbClientTotal2018() {
    //nb clients total
    $cnx = getBdd();
    $sql = "SELECT COUNT(client.code_c) FROM client, commande WHERE client.code_c = commande.code_c AND date_commande LIKE '%2018%' ";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
function nbClientTotal2019() {
    //nb clients total
    $cnx = getBdd();
    $sql = "SELECT COUNT(client.code_c) FROM client, commande WHERE client.code_c = commande.code_c AND date_commande LIKE '%2019%' ";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
/****************************************  Nombres Clients ****************************************/


/****************************************  Chiffre Affaires  ****************************************/
function chiffreAffaireTotal2017() {
    //nb CA total
    $cnx = getBdd();
    $sql = "SELECT SUM(`total_ht`) FROM `commande` WHERE `date_commande` LIKE '%2017%'";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
function chiffreAffaireTotal2018() {
    //nb CA total
    $cnx = getBdd();
    $sql = "SELECT SUM(`total_ht`) FROM `commande` WHERE `date_commande` LIKE '%2018%'";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
function chiffreAffaireTotal2019() {
    //nb CA total
    $cnx = getBdd();
    $sql = "SELECT SUM(`total_ht`) FROM `commande` WHERE `date_commande` LIKE '%2019%'";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
/****************************************  Chiffre Affaires  ****************************************/

/****************************************  Moyenne Commande  ****************************************/
function moyenneCommande2017() {
    //nb moyenne total
    $cnx = getBdd();
    $sql = "SELECT ROUND(AVG(ligne_commande.quantite_demandee),2) FROM ligne_commande , commande WHERE commande.numero = ligne_commande.numero AND commande.date_commande LIKE '%2017%'";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
function moyenneCommande2018() {
    //nb moyenne total
    $cnx = getBdd();
    $sql = "SELECT ROUND(AVG(ligne_commande.quantite_demandee),2) FROM ligne_commande , commande WHERE commande.numero = ligne_commande.numero AND commande.date_commande LIKE '%2018%'";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
function moyenneCommande2019() {
    //nb moyenne total
    $cnx = getBdd();
    $sql = "SELECT ROUND(AVG(ligne_commande.quantite_demandee),2) FROM ligne_commande , commande WHERE commande.numero = ligne_commande.numero AND commande.date_commande LIKE '%2019%'";
    $idRequete= executeRequete($cnx, $sql);
    return $idRequete;
}
/****************************************  Moyenne Commande  ****************************************/