<?php

$tpl = new Smarty();
// Pour header.tpl
$tpl->assign('nomAffiche', $_SESSION['prenom'] . ' ' . $_SESSION['nom']);
$tpl->assign('title', 'Gourmandise | Commande');

if (isset($parametre['action'])) {

    switch ($parametre['action']) {

        /*         * ********************************************CONSULTATION********************************************* */
        case 'form_consulter':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Commande : Consultation");
            $tpl->assign("titreEtat", "Etat de la commande");
            $tpl->assign("readonlyON", "readonly='readonly'");
            $tpl->assign("valAction", "");
            $tpl->assign("valBtnAction", "");
            $tpl->assign("nbCommande", "0");
            $tpl->assign("message", $message);
            $tpl->assign("btnValide", "Valider la commande");
            $tpl->assign("btnInvalide", "Supprimer la validation");
            chargementFiche($tpl, $idRequete);
            chargementFicheEtat($tpl, $idEtat);
            chargementFicheTTC($tpl, $idTTC);

            /* Chargement des données ligne_commande */
            $listeLigneCommande = array();
            $i = 0;
            while ($row = $idLigne->fetch()) {
                $listeLigneCommande[$i]['numero_ligne'] = $row['numero_ligne'];
                $listeLigneCommande[$i]['reference'] = $row['reference'];
                $listeLigneCommande[$i]['designation'] = $row['designation'];
                $listeLigneCommande[$i]['quantite_demandee'] = $row['quantite_demandee'];
                $listeLigneCommande[$i]['prix_unitaire_ht'] = $row['prix_unitaire_ht'];
                $i++;
            }
            $tpl->assign('listeLigneCommande', $listeLigneCommande);
            /* Chargement des données ligne_commande */
            break;
        /*         * ********************************************CONSULTATION********************************************* */



        /*         * ********************************************MODIFICATION********************************************* */
        case 'form_modifier':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Commande : Modification");
            $tpl->assign("titreEtat", "Etat de la commande");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "modifier");
            $tpl->assign("valBtnAction", "Modifier");
            $tpl->assign("message", $message);
            chargementFiche($tpl, $idRequete);
            chargementFicheEtat($tpl, $idEtat);
            chargementFicheTTC($tpl, $idTTC);
            chargementMessageErreur($tpl, $message);

            /* Chargement des données ligne_commande */
            $listeLigneCommande = array();
            $i = 0;
            while ($row = $idLigne->fetch()) {
                $listeLigneCommande[$i]['numero_ligne'] = $row['numero_ligne'];
                $listeLigneCommande[$i]['reference'] = $row['reference'];
                $listeLigneCommande[$i]['designation'] = $row['designation'];
                $listeLigneCommande[$i]['quantite_demandee'] = $row['quantite_demandee'];
                $listeLigneCommande[$i]['prix_unitaire_ht'] = $row['prix_unitaire_ht'];
                $i++;
            }
            $tpl->assign('listeLigneCommande', $listeLigneCommande);
            break;
        /*         * ********************************************MODIFICATION********************************************* */

        case 'form_ajouter':
            // Pour ajouter
            $tpl->assign("titreForm", "Fiche Commande : Ajouter");
            $tpl->assign("readonlyON", "readonly='readonly'");
            $tpl->assign("valAction", "ajouter");
            $tpl->assign("valBtnAction", "Ajouter");
            $tpl->assign("message", $message);
            
            
            /* PANIER */
            $panier = new panier();
            /* PANIER */


            /* Chargement des données ligne_commande */
            $commandeListeProduit = array();
            $i = 0;
            while ($row = $idCommandeListeProduit->fetch()) {
                $commandeListeProduit[$i]['reference'] = $row['reference'];
                $commandeListeProduit[$i]['designation'] = $row['designation'];
                $commandeListeProduit[$i]['stock'] = $row['stock'];
                $commandeListeProduit[$i]['prix_unitaire_HT'] = $row['prix_unitaire_HT'];
                $i++;
            }
            $tpl->assign('commandeListeProduit', $commandeListeProduit);
            /* Chargement des données ligne_commande */


            break;


        /*         * ********************************************MODIFICATION********************************************* */
        case 'modifier':
//            // Pour corps de page
//            $tpl->assign("titreForm", "Fiche Commande : Consultation");
//            $tpl->assign("titreEtat", "Etat de la commande");
//            $tpl->assign("readonlyON", "");
//            $tpl->assign("valAction", "modifier");
//            $tpl->assign("valBtnAction", "Modifier");
//            $tpl->assign("message", $message);
//            chargementFiche($tpl, $idRequete);
//            chargementFicheEtat($tpl, $idEtat);
//            chargementFicheTTC($tpl, $idTTC);
//
//            /* Chargement des données ligne_commande */
//            $listeLigneCommande = array();
//            $i = 0;
//            while ($row = $idLigne->fetch()) {
//                $listeLigneCommande[$i]['numero_ligne'] = $row['numero_ligne'];
//                $listeLigneCommande[$i]['reference'] = $row['reference'];
//                $listeLigneCommande[$i]['designation'] = $row['designation'];
//                $listeLigneCommande[$i]['quantite_demandee'] = $row['quantite_demandee'];
//                $listeLigneCommande[$i]['prix_unitaire_ht'] = $row['prix_unitaire_ht'];
//                $i++;
//            }
//            $tpl->assign('listeLigneCommande', $listeLigneCommande);
//            /* Chargement des données ligne_commande */
            break;
        /*         * ********************************************MODIFICATION********************************************* */

        case 'ajouter':
            // Cas uniquement en retour d'ajout !
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Commande : Création");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "ajouter");
            $tpl->assign("valBtnAction", "Ajouter");
            $tpl->assign("listeCommande", "");
            //chargementMessageErreur($tpl, $message);
            
            
            $tpl->display('mod_commande/vue/commandeAjoutVue.tpl');
            break;
    }
    //Affichage des fiches
    $tpl->display('mod_commande/vue/commandeFicheVue.tpl');
} else {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alert">' . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }
    // Chargement des données commandes
    $listeCommande = array();
    $i = 0;
    while ($row = $idRequete->fetch()) {
        $listeCommande[$i]['numero'] = $row['numero'];
        $listeCommande[$i]['login'] = $row['login'];
        $listeCommande[$i]['nom'] = $row['nom'];
        $listeCommande[$i]['total_ht'] = $row['total_ht'];
        $listeCommande[$i]['valide'] = $row['valide'];
        $i++;
    }
    // Pour corps de page
    $tpl->assign("titreForm", "Liste des commandes");
    $tpl->assign('listeCommande', $listeCommande);
    //Affichage des listes
    $tpl->display('mod_commande/vue/commandeListeVue.tpl');
}





/* * ********AFFICHE LA CONSULTATION********* */

function chargementFiche($tpl, $idRequete) {
    $row = $idRequete->fetch();
    $tpl->assign("numero", $row['numero']);
    $tpl->assign("login", $row['login']);
    $tpl->assign("code_c", $row['code_c']);
    $tpl->assign("nom", $row['nom']);
}

function chargementFicheEtat($tpl, $idEtat) {
    $row = $idEtat->fetch();
    $tpl->assign("date_commande", $row['date_commande']);
    $tpl->assign("date_livraison", $row['date_livraison']);
    $tpl->assign("total_ht", $row['total_ht']);
    $tpl->assign("valide", $row['valide']);
}

function chargementFicheTTC($tpl, $idTTC) {
    $row = $idTTC->fetch();
    $tpl->assign("total_ht", $row['total_ht']);
    $tpl->assign("total_tva", $row['total_tva']);
    $tpl->assign("total_ttc", $row['total_ttc']);
}

/* * ********AFFICHE LA CONSULTATION********* */

function chargementFicheEtatRetourAction($tpl, $parametre) {

    $tpl->assign("date_commande", $parametre['date_commande']);
    $tpl->assign("date_livraison", $parametre['f_date_livraison']);
    $tpl->assign("total_ht", $parametre['total_ht']);
    $tpl->assign("valide", $parametre['valide']);
}

/* AJOUT */

function chargementFicheVierge($tpl) {

    $tpl->assign("reference", "");
    $tpl->assign("designation", "");
    $tpl->assign("quantite", "");
    $tpl->assign("descriptif", "");
    $tpl->assign("prix_unitaire_HT", "");
    $tpl->assign("stock", "");
    $tpl->assign("poids_piece", "");
}

/* AJOUT */


/* FONCTION MESSAGE */

function chargementMessage($tpl, $message) {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alert">'
                . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }
}

function chargementMessageErreur($tpl, $message) {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alertErreur">'
                . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }
}

/* FONCTION MESSAGE */
/* PANIER */

class panier {

    public function __construct() {
        if(!isset($_SESSION)){
            session_start();
        }
        if(!isset($_SESSION['panier'])){
            $_SESSION['panier'] = array();
        }
    }
}
/* PANIER */