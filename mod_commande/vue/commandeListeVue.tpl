<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title>{$title}</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="template/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


        <!-- Custom styling plus plugins -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                {include file='template/production/leftNavBar.tpl'}
                {include file='template/production/topNavBar.tpl'}

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-sm-4">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=commande">Commandes</a></li>
                                            <li class="active">{$titreForm}</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre-->  
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Listes des Commandes</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <form class="pos-ajout" method="POST" action="index.php">
                                            <input type="hidden" name="gestion" value="commande">
                                            <input type="hidden" name="action" value="form_ajouter">
                                            <label>Passer une Commande : <input id="aImage" type="image" name="btn_ajouter" src='template/images/icones/a16.png'></label>
                                        </form>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <table id="datatable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Numéros</th>
                                                <th>Vendeur</th>
                                                <th>Client</th>
                                                <th>Montant HT en €</th>
                                                <th class="pos-actions">Consulter</th>
                                                <th class="pos-actions">Modifier</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach from=$listeCommande item=commande}

                                                <tr>
                                                    <td>{$commande.numero}</td>
                                                    <td>{$commande.login}</td>
                                                    <td>{$commande.nom}</td>
                                                    <td>{$commande.total_ht}</td>
                                                    <td class="pos-actions">
                                                        <form method="POST" action="index.php">
                                                            <input type="hidden" name="gestion" value="commande">
                                                            <input type="hidden" name="action" value="form_consulter">
                                                            <input type="hidden" name="f_numero" value="{$commande.numero}">
                                                            <input id="pImage" type="image" name="btn_consulter" src='template/images/icones/p16.png'>
                                                        </form>
                                                    </td>
                                                    <!--FAUT UN IF ICI-->
                                                    {if $commande.valide == 0}
                                                        <td class="pos-actions">
                                                            <form method="POST" action="index.php">
                                                                <input type="hidden" name="gestion" value="commande">
                                                                <input type="hidden" name="action" value="form_modifier">
                                                                <input type="hidden" name="f_numero" value="{$commande.numero}">
                                                                <input id="mImage" type="image" name="btn_modifier" src='template/images/icones/m16.png'>
                                                            </form>
                                                        </td>
                                                        {else}
                                                        <td class="pos-actions">
                                                            <p>Validée</p>
                                                        </td>
                                                    {/if}

                                                    <!--FAUT UN IF ICI-->
                                                </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
                <!-- /page content -->

                {include 'template/production/footerBar.tpl'}
            </div>
        </div>

        <!-- jQuery -->
        <script src="template/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="template/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="template/vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../../vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="template/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="template/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="template/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="template/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="template/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="template/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="template/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="template/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="template/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="template/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="template/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="template/vendors/jszip/dist/jszip.min.js"></script>
        <script src="template/vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="template/vendors/pdfmake/build/vfs_fonts.js"></script>


        <!-- Custom Theme Scripts -->
        <script src="template/build/js/custom.min.js"></script>
    </body>
</html>