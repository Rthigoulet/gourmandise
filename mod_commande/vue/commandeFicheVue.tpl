<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title>{$title}</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="template/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="template/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            {include 'template/production/leftNavBar.tpl'}
            <div class="main_container">
                {include 'template/production/topNavBar.tpl'}
                <div class="right_col" role="main">
                    <div class="">

                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-sm-4">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=commande">Commandes</a></li>
                                            <li class="active">{$titreForm}</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre-->        
                        {if $valAction!= 'ajouter'}
                            <!--Fiche Commande-->
                            <div class="row">
                                <div class=" pull-left col-md-5 col-sm-12 col-xs-12 ">
                                    <div class="x_panel fixed_height_320">
                                        <div class="x_title">
                                            <h2>{$titreForm}</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div style="text-align: center;">
                                            <br />
                                            <form action="index.php" method="POST" class="form-horizontal">
                                                <div class="form-group">
                                                    <h2><b>Numero</b> : {$numero}</h2>
                                                </div>
                                                <div class="form-group">
                                                    <h2><b>Vendeur</b> : {$login}</h2>
                                                </div>
                                                <div class="form-group">
                                                    <h2><b>Code Client</b> : {$code_c}</h2>
                                                </div>
                                                <div class="form-group">
                                                    <h2><b>Nom du Client</b> : {$nom}</h2>
                                                </div>
                                                <div class="ln_solid"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                                <!--BTN VALIDATION-->
                                <div class="col-md-2 col-sm-12 col-xs-12">
                                    {if $valAction == ''}
                                        {if $valide == 0}
                                            <input type="submit" id="f_btn-action" class="col-md-12 col-sm-12 col-xs-12 btn btn-submit btn-dark" value="{$btnValide}">
                                        {else}
                                            <input type="submit" id="f_btn-action" class="col-md-12 col-sm-12 col-xs-12 btn btn-submit btn-dark" value="{$btnInvalide}">
                                        {/if}
                                    {/if}
                                </div>
                                <!--BTN VALIDATION-->


                                <!--Fiche Commande-->
                                <!--Fiche ETAT-->
                                <div class="pull-right col-md-5 col-sm-12 col-xs-12 ">
                                    <div class="x_panel fixed_height_320">
                                        <div class="x_title">
                                            <h2>{$titreEtat}</h2>
                                            <a class="pull-right">{$message}</a>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div style="text-align: center;">
                                            <br />
                                            <form action="index.php" method="POST" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                                <input type="hidden" name="gestion"  value="commande" >
                                                <input type="hidden" name="action"  value="{$valAction}" >


                                                <input type="hidden" name="f_numero"  class="form-control" readonly="readonly" value="{$numero}">

                                                <div class="form-group">
                                                    <h2><b>Date Commande</b> : {$date_commande}</h2>
                                                </div>


                                                <!--Seule endroit qui modifie-->
                                                {*{if $valAction != 'modifier'}*}
                                                <div class="form-group">
                                                    <h2><b>Date Livraison</b> : {$date_livraison}</h2>
                                                </div>
                                                {*{else}
                                                <div class="form-group">
                                                <label for="date_livraison" class="control-label col-md-3 col-sm-3 col-xs-12">Date Livraison</label>
                                                <div class="col-md-4 col-sm-4 col-xs-6">
                                                <input name="f_date_livraison" class="form-control col-md-7 col-xs-12" value="{$date_livraison}" {$readonlyON} type="date" > 
                                                </div>
                                                </div>
                                                {/if}*}
                                                <!--Seule endroit qui modifie-->    


                                                <div class="form-group">
                                                    <h2><b>Total HT</b> : {$total_ht} € HT</h2>
                                                </div>
                                                <div class="form-group">
                                                    <h2><b>Commande Validée</b> : 
                                                        {if {$valide} == "1"}
                                                            OUI
                                                        {else}
                                                            NON
                                                        {/if}
                                                    </h2>
                                                </div>
                                                <div class="ln_solid">
                                                    {*{if $valBtnAction != ""}
                                                    <input type="submit" id="f_btn-action" class="btn btn-submit btn-dark" value="{$valBtnAction}" >
                                                    {/if}*}
                                                </div>                                     
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--Fiche ETAT-->


                                <!--Fiche PRIX TOTAL COMMANDE-->
                                <div class="row">
                                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-euro"></i>
                                            </div>
                                            <div class="count">{$total_ttc}</div>

                                            <h3>Montant Total de la Commande</h3>
                                            <p>Toute Taxe Comprise en €</p>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-caret-square-o-down"></i>
                                            </div>
                                            <div class="count">{$total_ht}</div>

                                            <h3>Montant Hors-Taxe de la Commande</h3>
                                            <p>Hors-Taxe en €</p>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-caret-square-o-up"></i>
                                            </div>
                                            <div class="count">{$total_tva}</div>

                                            <h3>Montant de la TVA de la Commande</h3>
                                            <p>Taxe sur la Valeur Ajoutée en €</p>
                                        </div>
                                    </div>
                                </div>
                                <!--Fiche PRIX TOTAL COMMANDE-->

                                <!--Fiche Ligne-->
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Détail de la Commande</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <table id="datatable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>N°Ligne</th>
                                                        <th>Référence</th>
                                                        <th>Désignation</th>
                                                        <th>Quantité</th>
                                                        <th>Prix HT en €</th>
                                                            {if $valAction == "modifier"}
                                                            <th>Modifier ?</th>
                                                            {/if}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {foreach from=$listeLigneCommande item=ligneC}
                                                        <tr>
                                                            <td>{$ligneC.numero_ligne}</td>
                                                            <td>{$ligneC.reference}</td>
                                                            <td>{$ligneC.designation}</td>
                                                            <!--Seule endroit qui modifie-->
                                                            {if $valAction == "modifier"}
                                                                <td>
                                                                    <div class="form-group">
                                                                        <label for="quantite_demandee"></label>
                                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                            <input type="hidden" name="f_numero_ligne"  class="form-control" readonly="readonly" value="{$ligneC.numero_ligne}"><!--Indispensable pour savoir quelle numero de ligne est sélectionné-->
                                                                            <input name="f_quantite_demandee" class="form-control col-md-2 col-xs-2" value="{$ligneC.quantite_demandee}" {$readonlyON} type="number" > 
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            {else}
                                                                <td>{$ligneC.quantite_demandee}</td>
                                                            {/if}
                                                            <!--Seule endroit qui modifie-->
                                                            <td>{$ligneC.prix_unitaire_ht}</td>

                                                            {if $valAction == "modifier"}
                                                                <td>
                                                                    <div class="form-group">

                                                                        <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="{$valBtnAction}">

                                                                    </div>
                                                                </td>
                                                            {/if}
                                                        </tr>
                                                    {/foreach}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--Fiche Ligne-->
                                <div>                 
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=commande"'>
                                            <button class="btn btn-info pull-right" onclick="window.print();"><i class="fa fa-print"></i> Imprimer</button>
                                        </div>
                                    </div>

                                </div> 

                            </div>
                        {else}











                            <!--AJOUTER-->
                            <div class="">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Listes des Produits</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <table id="datatable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Référence</th>
                                                        <th>Désignation</th>                                                  
                                                        <th>Prix Unitaire HT en €</th>
                                                        <th>Stock</th>                                                        
                                                        <th>Quantité</th>
                                                        <th class="pos-actions">Ajouter</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {foreach from=$commandeListeProduit item=listeCommande}
                                                        <tr>
                                                            <td>{$listeCommande.reference}</td>
                                                            <td>{$listeCommande.designation}</td>
                                                            <td>{$listeCommande.prix_unitaire_HT}</td>
                                                            <td>{$listeCommande.stock}</td>
                                                            <td>
                                                                <input name="f_quantite_prod" class="form-control col-md-7 col-xs-12" value="" type="quantite_prod" >
                                                            </td>
                                                            <td class="pos-actions">
                                                                <form method="POST" action="index.php">
                                                                    <input type="hidden" name="gestion" value="commande">
                                                                    <input type="hidden" name="action" value="form_ajouter">
                                                                    <input type="hidden" name="f_reference" value="{$listeCommande.reference}">
                                                                    <input id="pImage" type="image" name="btn_ajouter" src='template/images/icones/m16.png'>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    {/foreach}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
                        <!--AJOUTER-->












                        <br>
                        <br>
                        <br>
                        <br>

                    </div>
                </div>
            </div>
        </div>
        {include 'template/production/footerBar.tpl'}
    </div>
    <!-- jQuery -->
    <script src="template/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="template/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="template/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="template/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="template/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="template/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="template/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="template/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="template/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="template/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="template/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="template/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="template/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="template/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="template/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="template/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="template/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="template/vendors/jszip/dist/jszip.min.js"></script>
    <script src="template/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="template/vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Custom Theme Scripts -->
    <!-- Skycons -->
    <script src="template/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="template/vendors/Flot/jquery.flot.js"></script>
    <script src="template/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="template/vendors/Flot/jquery.flot.time.js"></script>
    <script src="template/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="template/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="template/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="template/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="template/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="template/vendors/moment/min/moment.min.js"></script>
    <script src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="template/build/js/custom.min.js"></script>

</body>
</html>
