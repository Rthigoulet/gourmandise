<?php

function liste() {
    $cnx = getBdd();
    $sql = "SELECT commande.numero ,client.nom , vendeur.login, commande.total_ht, commande.valide FROM commande , vendeur, client WHERE commande.code_v = vendeur.code_v  AND commande.code_c = client.code_c ORDER BY commande.numero DESC";
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}

/* * ********AFFICHE LA CONSULTATION********* */

function fiche($parametre) {
    $cnx = getBdd();
    $sql = "SELECT Commande.numero, Client.nom, Client.code_c, Vendeur.login "
            . "FROM Commande "
            . "INNER JOIN Vendeur "
            . "ON Commande.code_v = Vendeur.code_v "
            . "INNER JOIN Client "
            . "ON Commande.code_c = Client.code_c "
            . "WHERE Commande.numero = ?";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_numero']));
    return $idRequete;
}

function ficheEtat($parametre) {
    $cnx = getBdd();
    $sql = "SELECT DATE_FORMAT(date_commande,'%d / %m / %Y') AS date_commande, DATE_FORMAT(date_livraison ,'%d / %m / %Y') AS date_livraison, total_ht, valide FROM Commande WHERE Commande.numero = ?";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_numero']));
    return $idRequete;
}

function ficheLigne($parametre) {
    $cnx = getBdd();
    $sql = "SELECT Ligne_commande.numero_ligne, Produit.reference, Produit.designation, Ligne_commande.quantite_demandee, Produit.prix_unitaire_ht FROM Ligne_commande INNER JOIN produit ON Ligne_commande.reference = Produit.reference INNER JOIN Commande ON Commande.numero = Ligne_commande.numero WHERE Commande.numero = ?";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_numero']));
    return $idRequete;
}

function ficheTTC($parametre) {
    $cnx = getBdd();
    $sql = "SELECT Commande.total_ht, Commande.total_tva, SUM(total_ht + total_tva) AS total_ttc FROM Commande WHERE Commande.numero = ?";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_numero']));
    return $idRequete;
}

/* * ********AFFICHE LA CONSULTATION********* */

/* * ********Modifier LA COMMANDE********* */

//function modifierEtat($parametre) {
//    $cnx = getBdd();
//    $sql = "UPDATE Commande SET date_livraison = ? WHERE numero = ?";
//    $idRequete = executeRequete($cnx, $sql, array($parametre['f_date_livraison'],
//        $parametre['f_numero']));
//    return $idRequete;
//}

function modifier($parametre) {
    $cnx = getBdd();
    $sql = "UPDATE Ligne_commande SET quantite_demandee = ? WHERE numero = ? AND numero_ligne = ?";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_quantite_demandee'],$parametre['f_numero'], $parametre['f_numero_ligne']));
    return $idRequete;
}

function modifierValidation($parametre) {
    $cnx = getBdd();
    $sql = "UPDATE Commande SET valide = ? WHERE numero = ?";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_valide'],$parametre['f_numero']));
    return $idRequete;
}

/* * ********Modifier LA COMMANDE********* */


/* A FAIRE POUR AJOUT  */

function CommandeListeProduit() {
    $cnx = getBdd();
    $sql = "SELECT * FROM produit";
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}

//function ajouter($parametre) {
//
//    $cnx = getBdd();
//
//    $id = intval($parametre['f_code_c']);
//
//    $sql = "INSERT INTO client VALUES(?,?,?,?,?,?)";
//    $idRequete = executeRequete($cnx, $sql, array($id, $parametre['f_nom'], $parametre['f_adresse'],
//        $parametre['f_cp'], $parametre['f_ville'],
//        $parametre['f_telephone']));
//    return $idRequete;
//}

/*A FAIRE POUR AJOUT*/