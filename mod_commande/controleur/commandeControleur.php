<?php

define('GESTION', $gestion);
require_once('mod_' . $gestion . '/modele/' . $gestion . 'Modele.php');

//Traitement de la liste
function vueParDefaut($message = null) {

    $idRequete = liste();
    // Chargement de la liste par défaut

    require_once('mod_' . GESTION . '/vue/' . GESTION . 'Vue.php');
}

//Traitement du chargement des fiches
function form_action($parametre, $message = null) {

    switch ($parametre['action']) {
        case 'form_consulter' :
        case 'form_modifier' :
        case 'form_supprimer' :
        case 'modifier' :
            $idModifValid = modifierValidation($parametre);
        case 'supprimer' :
            //Partie statistique à traiter
            
            $idRequete = fiche($parametre);
            $idEtat = ficheEtat($parametre);
            $idTTC = ficheTTC($parametre);
            $idLigne = ficheLigne($parametre); //Liste des ligne_commande dans consulter et modifier Commande
            break;
        case 'form_ajouter' :
            $idCommandeListeProduit = CommandeListeProduit(); //Liste des produits dans ajouter Commande
            break;
    }
    require_once('mod_' . GESTION . '/vue/' . GESTION . 'Vue.php');
}

//Traitement de l'action en écriture
function action($parametre, $message = "") {

    //Pour le contrôle
    switch ($parametre['action']) {

        case 'ajouter' :
            $message = controleSaisie($parametre);
            break;
        case 'modifier' :
            $message = controleSaisie($parametre);
            break;
        case 'supprimer' :
            $idRequete = controleSuppression($parametre);

            if ($idRequete->rowCount() > 0) {
                $message = "Suppression impossible.";
                form_action($parametre, $message);
            }
            break;
    }
    if (!empty($message)) {
        form_action($parametre, $message);
    } else {
        $t = $parametre['action'];
        //Traitement de l'action
        $idRequete = $t($parametre);

        if ($idRequete) {
            switch ($parametre['action']) {

                case 'modifier' :
                    $msg = "Modification effectuée avec succès !";
                    break;

                case 'ajouter' :
                    $msg = "Ajout effectué avec succès !";
                    break;

                case 'supprimer' :
                    $msg = "Suppression effectuée  avec succès !";
                    break;
            }
        } else {
            $msg = "Problème lors de l'execution de la dernière action";
        }
        vueParDefaut($msg);
    }
}
