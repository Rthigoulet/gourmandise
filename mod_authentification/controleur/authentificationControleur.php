<?php
require 'mod_authentification/modele/authentificationModele.php';

function vueParDefaut($messsage = null) {

    require_once 'mod_authentification/vue/authentificationVue.php';
}

function deconnexion($message = null) {
    
    // Supression des variables de session et de la session
    $_SESSION = array();
    session_destroy();

    header('Location: index.php');
    exit;
}

function authentification($parametre, $message = "") {
   
    
    $idRequete = authentificationExist($parametre);
    
    if($idRequete->rowcount()== 1){
        $row = $idRequete->fetch(PDO::FETCH_NUM);
        $temp = $parametre['motDePasse'];
        
        /*Cryptage & décryptage*/
        $gauche = 'ar30&y%';
        $droite = 'tk!@';
        $jeton = hash('ripemd128', "$gauche$temp$droite");
        /*Cryptage & décryptage*/
        
        //$jeton = $temp; // Si jamais pas de cryptage
        if($jeton == $row[8]){
            session_start();
            /*ROW à changer selon l'emplacement*/
            $_SESSION['login'] = $row[7];
            $_SESSION['prenom'] = $row[2];
            $_SESSION['nom'] = $row[1];
            $_SESSION['code_v'] = $row[0];
            
            header('location:index.php');
            
        }else{
          $message = "Mot de passe incorrect";
          require_once 'mod_authentification/vue/authentificationVue.php';  

        }
        
    }else{
        $message = "Identifiant incorrect";
        require_once 'mod_authentification/vue/authentificationVue.php';
    } 
}