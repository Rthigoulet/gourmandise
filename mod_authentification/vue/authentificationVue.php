<?php

$tpl = new Smarty();
$tpl->assign('title','Gourmandise | Connexion');
$tpl->assign('date',date('Y'));



if (isset($parametre['action'])) {

    switch ($parametre['action']) {
        
    }
    chargementMessageErreur($tpl, $message);
    $tpl->display('mod_authentification/vue/authentificationVue.tpl');
} else {

    if (isset($message)) {
        chargementMessageErreur($tpl, $message);
        $tpl->assign('message', $message);
    } else {
        $tpl->assign('message', '');
    }
    $tpl->display('mod_authentification/vue/authentificationVue.tpl');
}


function chargementMessageErreur($tpl, $message) {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alertErreurLogin">'
                . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }
}