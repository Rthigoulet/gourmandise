<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title>{$title}</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- Animate.css -->
        <link href="template/vendors/animate.css/animate.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
        <link href="template/build/css/style.css" rel="stylesheet">
    </head>

    <body class="login">
        <div>
            <div class="login_wrapper">
                <div class="animate form login_form">
                    <a>{$message}</a>
                    <section style="" class="login_content">
                        
                        <form method="POST" action="index.php">
                            
                            <h1>Connexion</h1>
                            <input type="hidden" name="action" value="authentification">  {*name & value important*}

                            <div>
                                <input type="text" class="form-control" placeholder="Login" required="" name="login"/>  {*name important*}
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Mot de Passe" required="" name="motDePasse"/> {*name important*}
                            </div>
                            <div>
                                <input class="btn btn-dark btnConnexion" type="submit" value="Se Connecter" name="connexion"> {*href = index.html important*}
                            </div>
                        </form>
                        <div class="clearfix"></div>

                        <div class="separator">

                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1><i class="fa fa-graduation-cap"></i> Gourmandise CCI</h1>
                                <p>BTS SIO {$date}</p>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </body>
</html>
