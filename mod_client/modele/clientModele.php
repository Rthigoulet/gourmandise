<?php

function liste() {

    $cnx = getBdd();
    $sql = "SELECT * FROM client ";
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}


function fiche($parametre){
    
    $cnx = getBdd();
    $sql = "SELECT * FROM client WHERE code_c = ?";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_code_c']));
    return $idRequete;
}

function modifier($parametre){
    
    $cnx = getBdd();
    
    $sql = "UPDATE client SET nom = ?, adresse = ?, cp = ?, ville = ?,"
            . " telephone= ? WHERE code_c = ?";
    $idRequete = executeRequete($cnx, $sql,
            array($parametre['f_nom'],$parametre['f_adresse'],
                $parametre['f_cp'],$parametre['f_ville'],
                $parametre['f_telephone'],$parametre['f_code_c']));
    return $idRequete;
}

function ajouter($parametre){
    
    $cnx = getBdd();
    
    $id = intval($parametre['f_code_c']);
   
    $sql = "INSERT INTO client VALUES(?,?,?,?,?,?)";
    $idRequete = executeRequete($cnx, $sql,
            array($id,$parametre['f_nom'],$parametre['f_adresse'],
                $parametre['f_cp'],$parametre['f_ville'],
                $parametre['f_telephone']));
    return $idRequete;  
}

function supprimer($parametre){
    
    $cnx = getBdd();

    $sql = "DELETE FROM client WHERE code_c = ?";
    $idRequete = executeRequete($cnx, $sql,
            array($parametre['f_code_c']));
    
    return $idRequete;
    
    
}


/***** CONTROLE SUPPRESSION  *****/
function controleSuppression($parametre){
    $cnx = getBdd();

    $sql = "SELECT * FROM commande WHERE code_c = ?";
    $idRequete = executeRequete($cnx, $sql,
            array($parametre['f_code_c']));
    
    return $idRequete;
    
}

/***** RECHERCHES COMPLEMENTAIRES *****/

function stat01($parametre){
    //CA du client
    $cnx = getBdd();
    $sql = "SELECT code_c, SUM(total_ht) "
            . "FROM commande WHERE code_c = ? GROUP BY code_c";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_code_c']));
    return $idRequete;
}

function stat02($parametre){
    //Pour un calcul en % du CA réalisé
    $cnx = getBdd();
    $sql = "SELECT SUM(total_ht) FROM commande";
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}


function stat03($parametre){
    // 5 articles favoris du client
    $cnx = getBdd();
    $sql = "SELECT designation, 
        COUNT(ligne_commande.reference * quantite_demandee)
        FROM commande, ligne_commande, produit
        WHERE commande.numero = ligne_commande.numero
        AND ligne_commande.reference = produit.reference
        AND code_c = ?
        GROUP BY designation
        ORDER BY 2 DESC
        LIMIT 5";
    $idRequete = executeRequete($cnx, $sql, array($parametre['f_code_c']));
    return $idRequete;
}


