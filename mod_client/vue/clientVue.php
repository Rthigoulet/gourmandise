<?php

$tpl = new Smarty();

// Pour header.tpl
$tpl->assign('nomAffiche',$_SESSION['prenom']. ' ' .$_SESSION['nom']);
$tpl->assign('title','Gourmandise | Clients');

if (isset($parametre['action'])) {


    switch ($parametre['action']) {

        case 'form_consulter':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Client : Consultation");
            $tpl->assign("readonlyON", "readonly='readonly'");
            $tpl->assign("valAction", "");
            $tpl->assign("valBtnAction", "");
            $tpl->assign("nbProduit", "0");
            $tpl->assign("message", $message);
            chargementFiche($tpl, $idRequete);
            chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);

            break;

        case 'form_supprimer':
        case 'supprimer':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Client : Suppression");
            $tpl->assign("readonlyON", "readonly='readonly'");
            $tpl->assign("valAction", "supprimer");
            $tpl->assign("valBtnAction", "Supprimer");
            $tpl->assign("nbProduit", "0");
            $tpl->assign("message", $message);
            chargementFiche($tpl, $idRequete);
            chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);

            break;



        case 'form_modifier':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Client : Modification");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "modifier");
            $tpl->assign("valBtnAction", "Modifier");
            $tpl->assign("message", $message);
            chargementFiche($tpl, $idRequete);
            chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);

            break;

        case 'form_ajouter':
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Client : Création");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "ajouter");
            $tpl->assign("valBtnAction", "Ajouter");
            $tpl->assign("ca", "");
            $tpl->assign("listeProduits", "");
            $tpl->assign("partCa", "");
            $tpl->assign("message", $message);
            chargementFicheVierge($tpl);
            chargementMessageErreur($tpl, $message);
            break;

        case 'modifier':
            // Cas uniquement en retour de modification !
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Client : Modification");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "modifier");
            $tpl->assign("valBtnAction", "Modifier");
            chargementFicheRetourAction($tpl, $parametre);
            chargementStatistique($tpl, $idStat01, $idStat03, $idStat02);
            chargementMessageErreur($tpl, $message);

            break;


        case 'ajouter':
            // Cas uniquement en retour d'ajout !
            // Pour corps de page
            $tpl->assign("titreForm", "Fiche Client : Création");
            $tpl->assign("readonlyON", "");
            $tpl->assign("valAction", "ajouter");
            $tpl->assign("valBtnAction", "Ajouter");
            chargementFicheRetourAction($tpl, $parametre);
            $tpl->assign("ca", "");
            $tpl->assign("listeProduits", "");
            $tpl->assign("partCa", "");
            chargementMessageErreur($tpl, $message);

            break;
    }

    //Affichage des fiches
    $tpl->display('mod_client/vue/clientFicheVue.tpl');
} else {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alert">' . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }

    // Chargement des données clients 
    $listeClients = array();
    $i = 0;
    while ($row = $idRequete->fetch()) {
        $listeClients[$i]['code_c'] = $row['code_c'];
        $listeClients[$i]['nom'] = $row['nom'];
        $listeClients[$i]['ville'] = $row['ville'];
        $listeClients[$i]['telephone'] = $row['telephone'];

        $i++;
    }


    // Pour corps de page
    $tpl->assign("titreForm", "Liste des clients");

    $tpl->assign('listeClients', $listeClients);




    //Affichage des listes

    $tpl->display('mod_client/vue/clientListeVue.tpl');
}

function chargementFiche($tpl, $idRequete) {

    $row = $idRequete->fetch();
    $tpl->assign("code_c", $row['code_c']);
    $tpl->assign("nom", $row['nom']);
    $tpl->assign("adresse", $row['adresse']);
    $tpl->assign("cp", $row['cp']);
    $tpl->assign("ville", $row['ville']);
    $tpl->assign("telephone", $row['telephone']);
}

function chargementFicheRetourAction($tpl, $parametre) {

    $tpl->assign("code_c", $parametre['f_code_c']);
    $tpl->assign("nom", $parametre['f_nom']);
    $tpl->assign("adresse", $parametre['f_adresse']);
    $tpl->assign("cp", $parametre['f_cp']);
    $tpl->assign("ville", $parametre['f_ville']);
    $tpl->assign("telephone", $parametre['f_telephone']);
}

function chargementFicheVierge($tpl) {

    $tpl->assign("code_c", "");
    $tpl->assign("nom", "");
    $tpl->assign("adresse", "");
    $tpl->assign("cp", "");
    $tpl->assign("ville", "");
    $tpl->assign("telephone", "");
}

function chargementMessageErreur($tpl, $message) {

    if (isset($message)) {
        $tpl->assign("message", '<p class="pos-alertErreur">'
                . $message . '</p>');
    } else {
        $tpl->assign("message", "");
    }
}

function chargementStatistique($tpl, $idStat01, $idStat03, $idStat02) {

    //Chargement chiffre d'affaire du client
    $row = $idStat01->fetch(PDO::FETCH_NUM);
    $tpl->assign("ca", $row[1] . ' €');


    // Chargement les 5 produits les plus achetés pour 1 client

    if ($idStat03->rowCount() == 0) {
        $tpl->assign('listeProduits', 0);
    } else {
        $listeProduits = array();
        $i = 0;
        while ($ligne = $idStat03->fetch(PDO::FETCH_NUM)) {
            $listeProduits[$i]['designation'] = $ligne[0];
            $listeProduits[$i]['nbProduits'] = $ligne[1];
            $i++;
        }
        $tpl->assign('listeProduits', $listeProduits);
    }

    //Chargement CA de l'entreprise Gourmandise SARL'
    $res = $idStat02->fetch(PDO::FETCH_NUM);
    $caGlobal = $res[0];
    //Calcul de la part de CA du client pour Gourmandise
    $partCaClient = ($row[1] * 100) / $caGlobal;

    //Assignation des zones
    $tpl->assign("partCa", $partCaClient);
}