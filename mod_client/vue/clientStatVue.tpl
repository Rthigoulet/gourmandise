<div class="col-md-6">
    <div class="card">
        <div class="card-header"> <strong>Statistiques</strong></div>
        <div class="card-body card-block">
            <div class="form-group"><strong>CA Réalisé : </strong>{$ca|string_format:"%.2f"} €</div>
            <div class="form-group"><strong>Pourcentage du CA réalisé : </strong>{$partCa|string_format:"%.2f"} %</div> 
            <div class="form-group"><strong>Ses meilleurs achats : </strong></label>
                <ul class="square">
                    {if  $listeProduits eq 0}
                        <li> Aucun achat effectué.</li>
                        {else}
                            {foreach from=$listeProduits item=produit}
                            <li>{$produit.designation} : {$produit.nbProduits}</li>
                            {/foreach} 
                        {/if}

                </ul>

            </div>
        </div>
    </div>
</div>
