<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.ico" type="image/ico" />
        <link rel="icon" href="template/production/images/bonbon.png" />
        <title>{$title}</title>

        <!-- Bootstrap -->
        <link href="template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="template/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="template/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="template/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="template/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="template/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="nav-md">

        <div class="container body">
            {include 'template/production/leftNavBar.tpl'}
            <div class="main_container">

                {include 'template/production/topNavBar.tpl'}

                <div class="right_col" role="main">
                    <div class="">

                        <!--Titre-->
                        <div class="breadcrumbs">
                            <div class="col-sm-4">
                                <div class="page-header float-left">
                                    <div class="page-title">
                                        <h1><i class="fa fa-heart">  La gourmandise, ça se partage !</i></h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="page-header float-right">
                                    <div class="page-title">
                                        <ol class="breadcrumb text-right">
                                            <li><a href="index.php">Accueil</a></li>
                                            <li><a href="index.php?gestion=client">Clients</a></li>
                                            <li class="active">{$titreForm}</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Titre-->        

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>{$titreForm}</h2>
                                        <a class="pull-right">{$message}</a>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <br />
                                        <form action="index.php" method="POST" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                            <input type="hidden" name="gestion"  value="client" >
                                            <input type="hidden" name="action"  value="{$valAction}" >

                                            {if $valAction != 'ajouter'}
                                                <div class="form-group">
                                                    <label for="code_c" class="control-label col-md-3 col-sm-3 col-xs-12">Code Client : </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="f_code_c"  class="form-control" readonly="readonly" value="{$code_c}">
                                                    </div>
                                                </div>
                                            {else}
                                                <input type="hidden" name="f_code_c"  class="form-control" readonly="readonly" value="{$code_c}">
                                            {/if}

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nom">Nom & Prénom <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="f_nom" required="required" value="{$nom}" {$readonlyON} class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="adresse">Adresse <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" name="f_adresse"  required="required" value="{$adresse}" {$readonlyON} class="form-control col-md-7 col-xs-12">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="cp" class="control-label col-md-3 col-sm-3 col-xs-12">CP <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_cp" class="form-control col-md-7 col-xs-12" value="{$cp}" {$readonlyON} type="text" required="required" name="middle-name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="ville" class="control-label col-md-3 col-sm-3 col-xs-12">Ville <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_ville" class="text-uppercase form-control col-md-7 col-xs-12" value="{$ville}" {$readonlyON} type="text" required="required" name="middle-name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="telephone" class="control-label col-md-3 col-sm-3 col-xs-12">Téléphone <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input name="f_telephone" class="form-control col-md-7 col-xs-12" value="{$telephone}" {$readonlyON} required="required" type="text">
                                                </div>
                                            </div>
                                            <div class="ln_solid"></div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                    <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=client"'>
                                                    <button class="btn btn-primary" type="reset">Reset</button>
                                                    {if $valBtnAction != ""}<input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="{$valBtnAction}" >{/if}
                                                    <button class="btn btn-default pull-right" onclick="window.print();"><i class="fa fa-print"></i> Imprimer</button>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {if $valAction != 'ajouter'}
                        {include file='mod_client/vue/clientStatVue.tpl'}
                    {/if}
                </div>
            </div>
            {include 'template/production/footerBar.tpl'}
            <!-- jQuery -->
            <script src="template/vendors/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="template/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- FastClick -->
            <script src="template/vendors/fastclick/lib/fastclick.js"></script>
            <!-- NProgress -->
            <script src="template/vendors/nprogress/nprogress.js"></script>
            <!-- Chart.js -->
            <script src="template/vendors/Chart.js/dist/Chart.min.js"></script>
            <!-- gauge.js -->
            <script src="template/vendors/gauge.js/dist/gauge.min.js"></script>
            <!-- bootstrap-progressbar -->
            <script src="template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
            <!-- iCheck -->
            <script src="template/vendors/iCheck/icheck.min.js"></script>
            <!-- Skycons -->
            <script src="template/vendors/skycons/skycons.js"></script>
            <!-- Flot -->
            <script src="template/vendors/Flot/jquery.flot.js"></script>
            <script src="template/vendors/Flot/jquery.flot.pie.js"></script>
            <script src="template/vendors/Flot/jquery.flot.time.js"></script>
            <script src="template/vendors/Flot/jquery.flot.stack.js"></script>
            <script src="template/vendors/Flot/jquery.flot.resize.js"></script>
            <!-- Flot plugins -->
            <script src="template/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
            <script src="template/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
            <script src="template/vendors/flot.curvedlines/curvedLines.js"></script>
            <!-- DateJS -->
            <script src="template/vendors/DateJS/build/date.js"></script>
            <!-- JQVMap -->
            <script src="template/vendors/jqvmap/dist/jquery.vmap.js"></script>
            <script src="template/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
            <script src="template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
            <!-- bootstrap-daterangepicker -->
            <script src="template/vendors/moment/min/moment.min.js"></script>
            <script src="template/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

            <!-- Custom Theme Scripts -->
            <script src="template/build/js/custom.min.js"></script>

    </body>
</html>